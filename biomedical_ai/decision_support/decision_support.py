import numpy as np
nodes={'cognitive':{'cost':10,'value':75},
    'mri':{'cost':30,'value':68},
    'pet':{'cost':60,'value':65},
    'cognitive&mri':{'cost':40,'value':79},
    'mri&pet':{'cost':90,'value':76},
    'cognitive&pet':{'cost':70,'value':94},
    'cognitive&mri&pet':{'cost':100,'value':95},

}
def calculate_optimal_decision_chain(nodes):
    from itertools import combinations,permutations
    node_names=list(nodes.keys())
    coefficient=2
    unique=[]
    for n in node_names:
        if '&' not in n:
            unique.append(n)
    node_names_list=[]
    for chain in node_names:
        node_names_list.append(chain.split('&'))
    
    node_chain_combinations=list(permutations(unique, len(unique)))
    best_chain=None
    best_costvalue=-1*np.inf
    all_costvalues=[]
    
    for node_chain in node_chain_combinations:
        costvalue=0
        for i in range(len(node_chain)):
            current_chain=list(node_chain[0:i+1])
            for node_name_elem in node_names_list:
                if set(node_name_elem)==set(current_chain):
                    current_chain='&'.join(node_name_elem)
            costvalue+=(nodes[current_chain]['value']*coefficient)-nodes[current_chain]['cost']
        all_costvalues.append(costvalue)
        if costvalue > best_costvalue:
            best_chain=node_chain
            best_costvalue=costvalue

    print(node_chain_combinations)
calculate_optimal_decision_chain(nodes)
    
    
