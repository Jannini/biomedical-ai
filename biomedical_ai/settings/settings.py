import yaml
from yaml import CLoader as Loader
import os
dirname = os.path.dirname(__file__)

def get_settings(fname):
    settings=None
    if fname=='None':
        fname=f'{dirname}/settings.yaml'
    with open(fname,'r') as f:
        settings=yaml.load(f,Loader=Loader)
    return settings

def get_test_settings():
    settings=None
    fname=f'{dirname}/test_settings.yaml'
    with open(fname,'r') as f:
        settings=yaml.load(f,Loader=Loader)
    return settings

def get_eda_settings(fname):
    settings=None
    if fname=='None':
        fname=f'{dirname}/settings_eda.yaml'
    with open(fname,'r') as f:
        settings=yaml.load(f,Loader=Loader)
    return settings

def get_ssh_cluster_settings():
    sshcluster_settings=None
    with open(f'{dirname}/settings_sshcluster.yaml','r') as f:
        sshcluster_settings=yaml.load(f,Loader=Loader)
    return sshcluster_settings