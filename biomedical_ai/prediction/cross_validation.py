from sklearn.model_selection import RepeatedStratifiedKFold,RepeatedKFold,StratifiedKFold
#from biomedical_ai.inference.settings import DEFAULT_SETTINGS
from biomedical_ai.utils.utils import is_equal,Stratified1Fold,RepeatedStratifiedGroupKFold
def cross_validation_selector(settings):
    if is_equal(settings['CV TYPE'],'RepeatedStratifiedKFold'):
        return RepeatedStratifiedKFold(n_splits=settings['CV OUTER'],shuffle=True,random_state=0)
    elif is_equal(settings['CV TYPE'],'RepeatedKFold'):
        return RepeatedKFold(n_splits=settings['CV OUTER'],shuffle=True,random_state=0)
    elif is_equal(settings['CV TYPE'],'RepeatedStratifiedGroupKFold'):
        return RepeatedStratifiedGroupKFold(n_splits=settings['CV OUTER'],shuffle=True,random_state=0)
    elif is_equal(settings['CV TYPE'],'Stratified1Fold'):
        return Stratified1Fold(n_splits=settings['CV OUTER'],shuffle=True,random_state=0)
    elif is_equal(settings['CV TYPE'],'StratifiedKFold'):
        return StratifiedKFold(n_splits=settings['CV'],shuffle=True,random_state=0)