from scipy.sparse import data
from biomedical_ai.utils.utils import is_equal
from sklearn.preprocessing import StandardScaler,MinMaxScaler
import numpy as np
from scipy.stats import iqr as naniqr
class Normalizer():
    def __init__(self,settings) -> None:
        self.settings=settings
        self.model=None
    def normalize(self,X_train,X_test):
        X_train=self.fit_transform(X_train,self.settings)
        X_test=self.transform(X_test,self.settings)
        return X_train,X_test
    def fit_transform(self,data,settings=None):
        method=self.settings['NORMALIZATION']
        if is_equal(method,'normalization'):
            self.model=MinMaxScaler()
        elif is_equal(method,'standardization'):
            self.model=StandardScaler()
        elif is_equal(method,'knn'):
            pass
        elif is_equal(method,'autoencoder'):
            pass
        return self.model.fit_transform(data)
    def transform(self,data,settings):
        return self.model.transform(data)
class NonparametricScaler():
    def __init__(self):
        self.model={'median':[],'interquartile_range':[]}
    def fit(self,data):
        for i in range(len(data[1])):
            temp_data=data[:,i]
            self.model['median'].append(np.nanmedian(temp_data))
            self.model['interquartile_range'].append(naniqr(temp_data,non_policy='omit'))
    def fit_transform(self,data):
        new_data=[]
        for i in range(len(data[1])):
            temp_data=data[:,i]
            med=np.nanmedian(temp_data)
            iqr=naniqr(temp_data,non_policy='omit')
            self.model['median'].append(med)
            self.model['interquartile_range'].append(iqr)
            if iqr==0:iqr=1
            new_data.append((temp_data-med)/iqr)
        return np.asarray(new_data).T

    def transform(self,data):
        new_data=[]
        for i in range(len(data[1])):
            temp_data=data[:,i]
            med=self.model['median'][i]
            iqr=self.model['interquartile_range'][i]
            if iqr==0:iqr=1
            new_data.append((temp_data-med)/iqr)
        return np.asarray(new_data).T