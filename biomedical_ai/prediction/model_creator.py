from biomedical_ai.utils.utils import is_equal,calculate_valid_settings,load_data
from biomedical_ai.settings.settings import get_settings as default_settings
from biomedical_ai.settings.settings import get_ssh_cluster_settings as sshcluster_settings
from biomedical_ai.prediction.cross_validation import cross_validation_selector
from biomedical_ai.prediction import classifiers, feature_selection,regressors,clustering,surival_analysis
from biomedical_ai.prediction import metrics
from biomedical_ai.prediction import imputation,preprocess
from biomedical_ai.prediction.optimization import optuna,simple_opt
from biomedical_ai.utils.model_dataclass import ModelInfo
from biomedical_ai.interpretation.shap.ShapInterface import ShapInterface
import numpy as np
import mlflow
from time import time
from biomedical_ai.utils.logging import log_mlflow_cv_params,log_mlflow_settings,log_mlflow_global_scores,save_callback,COMPLETED_NUM



def create_run_models(settings,data=None,labels=None,supplement_data={}):
    """[summary]

    Args:
        data ([type], optional): [description]. Defaults to None.
        labels ([type], optional): [description]. Defaults to None.
        settings ([type], optional): [description]. Defaults to None.
        supplement_data (dict, optional): [description]. Defaults to {}.

    Returns:
        [type]: [description]
    """
    if settings==None:
        settings=default_settings
    parallel=settings['PARALLEL']

    ## INIT MLFLOW
    if settings['MLOPS']['LIBRARY']=='MLFLOW':
        mlflow.set_tracking_uri("http://127.0.0.1:5001")
        mlflow.set_experiment(settings['MLOPS']['EXPERIMENT NAME'])

    #INIT DISTRIBUTED
    parallel_futures=[]
    if is_equal(parallel,'Local'):
        from dask.distributed import Client,wait
        client = Client(threads_per_worker=1)
    elif is_equal(parallel,'Distributed'):
        from dask.distributed import Client,wait
        from biomedical_ai.utils.SSHCluster import SSHCluster
        cluster=SSHCluster(host_names=sshcluster_settings["host_addresses"],machine_names=sshcluster_settings["host_names"],usernames=sshcluster_settings["usernames"]
        ,passwords=sshcluster_settings["passwords"],local_dirs=sshcluster_settings["local_dirs"],conda_envs=sshcluster_settings["conda_envs"],
        worker_processes=sshcluster_settings["worker_threads"],conda_type=sshcluster_settings["conda_types"])
        cluster.create_cluster()
        time.sleep(10)
        client = Client('127.0.0.1:8786')   


    settings=calculate_valid_settings(settings)
    model_infos=[]
    for setting in settings:
        if setting['FILENAME']:
            data,labels,supplement_data=load_data(setting['FILENAME'])
        if is_equal(parallel,'None'):
            model_info=run_model_wrapper(data,labels,setting,supplement_data)
            #model_info.save()
        elif is_equal(parallel,'Local') or is_equal(parallel,'Distributed'):
            model_info=client.submit(run_model_wrapper,data,labels,setting,supplement_data)
            model_info.add_done_callback(save_callback)
            parallel_futures.append(model_info)
        model_infos.append(model_info)
        
    if is_equal(parallel,'Local') or is_equal(parallel,'Distributed'):
        wait(parallel_futures)
    return model_infos

def run_model_wrapper(data,labels,settings,supplement_data={}):
    """[summary]

    Args:
        data ([type]): [description]
        labels ([type]): [description]
        settings ([type]): [description]
        supplement_data (dict, optional): [description]. Defaults to {}.

    Returns:
        [type]: [description]
    """
    if settings['MLOPS']['LIBRARY']=='MLFLOW':
        with mlflow.start_run():
            model_info=run_model(data,labels,settings,supplement_data)
    else:
        model_info=run_model(data,labels,settings,supplement_data={})
    return model_info
def run_cv_iteration(train,test,data,labels,settings,supplement_data,scores,model_info):
    """[summary]

    Args:
        train ([type]): [description]
        test ([type]): [description]
        data ([type]): [description]
        labels ([type]): [description]
        settings ([type]): [description]
        supplement_data ([type]): [description]
        scores ([type]): [description]
        model_info ([type]): [description]

    Returns:
        [type]: [description]
    """
    use_imputation=settings['IMPUTATION']
    use_feature_selection=settings['FEATURE SELECTION']['METHOD']
    use_normalization=settings['NORMALIZATION']
    use_hyperparameter_optimization=settings['HYPERPARAMETER OPTIMIZATION']['METHOD']
    use_interpretation=settings['INTERPRETATION']

    prediction_type=settings['PREDICTION TYPE']

    hyperparameters=None
    X_train,X_test=data[train],data[test]
    y_train,y_test=labels[train],labels[test]
    if use_normalization:
            normalizer=preprocess.Normalizer(settings)
            X_train,X_test=normalizer.normalize(X_train,X_test)
    if use_imputation:
            imputer=imputation.Imputer(settings)
            X_train,X_test=imputer.impute(X_train,X_test)
    if use_feature_selection:
            feature_selector=feature_selection.FeatureSelector(settings)
            X_train,X_test=feature_selector.select_features(X_train,y_train,X_test)
    if use_hyperparameter_optimization:
            optimizer_library=_choose_hyperparamter_optimization_library(settings['HYPERPARAMETER OPTIMIZATION']['LIBRARY'])
            hyperparameter_optimizer=optimizer_library.HyperparameterOptimizer(settings)
            hyperparameters=hyperparameter_optimizer.optimize(X_train,y_train)
    predictor=_choose_predictor(prediction_type,settings)
    model=predictor.fit(X_train,y_train,hyperparameters)
    prediction=predictor.predict(X_test)
    scores=metrics.score(y_test,prediction,settings,scores)
    if is_equal(prediction_type,'classification'):
            probabilistic_prediction=predictor.predict_proba(X_test)
            #scores=metrics.score(y_test,probabilistic_prediction,settings,scores)

    if use_interpretation:
            shap=ShapInterface(model,X_test,settings,background_data=X_train,feature_names=supplement_data['feature_names'])
            shap_values=shap.compute_shap()
            model_info.shap_values.append(shap_values)


    model_info.scores=scores
        #model_info.predictions(prediction)
    model_info.models.append(model)
    if settings['MLOPS']['LIBRARY']=='MLFLOW':
            log_mlflow_cv_params(scores,hyperparameters)
    return model_info
def run_model(data,labels,settings,supplement_data={}):
    """[summary]

    Args:
        data ([type]): [description]
        labels ([type]): [description]
        settings ([type]): [description]
        supplement_data (dict, optional): [description]. Defaults to {}.

    Returns:
        [type]: [description]
    """
    model_info=ModelInfo(settings)
    if settings['MLOPS']['LIBRARY']=='MLFLOW':log_mlflow_settings(settings)
    try:
        model_info.feature_names=supplement_data['feature_names']
    except:
        supplement_data['feature_names']=np.arange(len(data[0]))
        model_info.feature_names=supplement_data['feature_names']

    scores={}
    skf=cross_validation_selector(settings)
    for train,test in skf.split(data,labels):
        if settings['MLOPS']['LIBRARY']=='MLFLOW':
            with mlflow.start_run(nested=True) as run:
                model_info=run_cv_iteration(train,test,data,labels,settings,supplement_data,scores,model_info)
        else:
            model_info=model_info=run_cv_iteration(train,test,data,labels,settings,supplement_data,scores,model_info)
    #calc_global_metrics()
    return model_info
def _choose_predictor(prediction_type,settings=None):
    """[summary]

    Args:
        prediction_type ([type]): [description]
        settings ([type], optional): [description]. Defaults to None.

    Returns:
        [type]: [description]
    """
    predictor=None
    if is_equal(prediction_type,'classification'):predictor=classifiers.Classifier(settings)
    elif is_equal(prediction_type,'regression'):predictor=regressors.Regressor(settings)
    elif is_equal(prediction_type,'clustering'):predictor=clustering.Cluster(settings)
    elif is_equal(prediction_type,'survival_analysis'):predictor=surival_analysis
    return predictor
def _choose_hyperparamter_optimization_library(opt_lib):
    """[summary]

    Args:
        opt_lib ([type]): [description]

    Returns:
        [type]: [description]
    """
    if is_equal(opt_lib,'optuna'):opt_lib=optuna
    elif is_equal(opt_lib,'simple_opt'):opt_lib=simple_opt
    elif is_equal(opt_lib,'bayesian'):opt_lib=clustering
    return opt_lib
