from scipy.sparse import data
from biomedical_ai.utils.utils import is_equal
from sklearn.impute import SimpleImputer,KNNImputer #IterativeImputer
class Imputer():
    def __init__(self,settings=None) -> None:
        self.settings=settings
        self.model=None
    def impute(self,X_train,X_test):
        X_train=self.fit_transform(X_train,self.settings)
        X_test=self.transform(X_test,self.settings)
        return X_train,X_test
    def fit_transform(self,data,settings=None):
        imputation_method=self.settings['IMPUTATION']
        if is_equal(imputation_method,'simple'):
            self.model=SimpleImputer(strategy="median")
        elif is_equal(imputation_method,'iterative'):
            self.model=IterativeImputer(initial_strategy='median',random_state=0)
        elif is_equal(imputation_method,'knn'):
            self.model=KNNImputer(n_neighbors=10)
        elif is_equal(imputation_method,'autoencoder'):
            pass
        return self.model.fit_transform(data)
    def transform(self,data,settings=None):
        return self.model.transform(data)
