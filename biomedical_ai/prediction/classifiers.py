from sklearn.svm  import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
#from biomedical_ai.settings import DEFAULT_SETTINGS
from catboost import CatBoostClassifier, Pool
from biomedical_ai.prediction.models.WeightedSubsetEnsemblePredictor import WeightedSubsetEnsembleClassifier
import xgboost as xgb
import numpy as np
import math
from biomedical_ai.utils.utils import is_equal


class Classifier():
    """[summary]
    """
    def __init__(self,settings=None) -> None:
        """[summary]

        Args:
            settings ([type], optional): [description]. Defaults to None.
        """
        self.settings=settings
        self.model=None
    def fit(self,data,labels,hyperparameters=None,settings=None):
        """[summary]

        Args:
            data ([type]): [description]
            labels ([type]): [description]
            hyperparameters ([type], optional): [description]. Defaults to None.
            settings ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """
        predictor=self.settings['PREDICTOR']
        hyperparams=self.settings['HYPERPARAMETER OPTIMIZATION']['METHOD']
        if is_equal(predictor,'catboost'):
            self.model=CatBoostClassifier()
            #train_data=Pool(data,labels,cat_features=[1,4,5,6,7,9,13])
            self.model.fit(data,labels,verbose=False)
        elif is_equal(predictor,'rforest'):
            if hyperparams:self.model=RandomForestClassifier(random_state=0,**hyperparameters)
            else:self.model=RandomForestClassifier(random_state=0)
            self.model.fit(data,labels)
        elif is_equal(predictor,'linsvm'):
            if hyperparams:self.model=SVC(kernel='linear',random_state=0,**hyperparameters)
            else:self.model=SVC(kernel='linear',random_state=0)
            self.model.fit(data,labels)
        elif is_equal(predictor,'polysvm'):
            if hyperparams:self.model=SVC(kernel='polynomial',random_state=0,**hyperparameters)
            else:self.model=SVC(kernel='polynomial',random_state=0,)
            self.model.fit(data,labels)
        elif is_equal(predictor,'sigsvm'):
            if hyperparams:self.model=SVC(kernel='sigmoid',random_state=0,**hyperparameters)
            else:self.model=SVC(kernel='sigmoid',random_state=0)
            self.model.fit(data,labels)
        elif is_equal(predictor,'rbfsvm'):
            if hyperparams:self.model=SVC(kernel='rbf',random_state=0,**hyperparameters)
            else:self.model=SVC(kernel='rbf',random_state=0)
            self.model.fit(data,labels)
        elif is_equal(predictor,'lda'):
            if hyperparams:self.model=LinearDiscriminantAnalysis(random_state=0,**hyperparameters)
            else:self.model=LinearDiscriminantAnalysis(random_state=0)
            self.model.fit(data,labels)
        elif is_equal(predictor,'logreg'):
            if hyperparams:self.model=LogisticRegression(random_state=0,**hyperparameters)
            else:self.model=LogisticRegression(random_state=0)
            self.model.fit(data,labels)
        elif is_equal(predictor,'xgboost'):
            if hyperparams:self.model=xgb(random_state=0,**hyperparameters)
            else:self.model=xgb(random_state=0)
            self.model.train(data,labels)
        elif is_equal(predictor,'fcdnn'):
            #if hyperparams:self.model=RandomForestClassifier(random_state=0,**hyperparameters)
            #else:self.model=
            self.model.fit(data,labels)
        elif is_equal(predictor,'cnndnn'):
            pass
        elif is_equal(predictor,'wefsp'):
            pass
        elif is_equal(predictor,'autosklearn'):
            pass
        elif is_equal(predictor,'tpot'):
            pass
        return self.model

    def predict(self,data):
        """[summary]

        Args:
            data ([type]): [description]

        Returns:
            [type]: [description]
        """
        predictor=self.settings['PREDICTOR']
        if is_equal(predictor,'Catboost'):
            #test_data=Pool()
            preds=self.model.predict(data,verbose=False)
        elif is_equal(predictor,'rforest'):
            pass
        elif is_equal(predictor,'linsvm'):
            pass
        elif is_equal(predictor,'polysvm'):
            pass
        elif is_equal(predictor,'sigsvm'):
            pass
        elif is_equal(predictor,'rbfsvm'):
            pass
        elif is_equal(predictor,'lda'):
            pass
        elif is_equal(predictor,'logreg'):
            pass
        elif is_equal(predictor,'xgboost'):
            pass
        elif is_equal(predictor,'fcdnn'):
            pass
        elif is_equal(predictor,'cnndnn'):
            pass
        elif is_equal(predictor,'wefsp'):
            pass
        elif is_equal(predictor,'autosklearn'):
            pass
        elif is_equal(predictor,'tpot'):
            pass
        return self.model.predict(data)
    def predict_proba(self,data):
        """[summary]

        Args:
            data ([type]): [description]

        Returns:
            [type]: [description]
        """
        predictor=self.settings['PREDICTOR']
        if is_equal(predictor,'Catboost'):
            #test_data=Pool()
            preds=self.model.predict(data,verbose=False)
        elif is_equal(predictor,'rforest'):
            pass
        elif is_equal(predictor,'linsvm'):
            pass
        elif is_equal(predictor,'polysvm'):
            pass
        elif is_equal(predictor,'sigsvm'):
            pass
        elif is_equal(predictor,'rbfsvm'):
            pass
        elif is_equal(predictor,'lda'):
            pass
        elif is_equal(predictor,'logreg'):
            pass
        elif is_equal(predictor,'xgboost'):
            pass
        elif is_equal(predictor,'fcdnn'):
            pass
        elif is_equal(predictor,'cnndnn'):
            pass
        elif is_equal(predictor,'wefsp'):
            pass
        elif is_equal(predictor,'autosklearn'):
            pass
        elif is_equal(predictor,'tpot'):
            pass
        return self.model.predict_proba(data)

