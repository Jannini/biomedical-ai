from sklearn.cluster import KMeans,AgglomerativeClustering
import som as Som


from biomedical_ai.utils.utils import is_equal

class Cluster():
    def __init__(self,settings=None) -> None:
        self.settings=settings
        self.model=None
    def fit(self,data,hyperparameters=None,settings=None):
        predictor=self.settings['PREDICTOR']
        cluster=self.settings['PREDICTOR PARAMETERS']['N_CLUSTERS']
        if is_equal(predictor,'knn'):
            model=KMeans(random_state=0,n_clusters=cluster,**hyperparameters)
        elif is_equal(predictor,'hierarchical'):
            model=AgglomerativeClustering(n_clusters=cluster,**hyperparameters)
        elif is_equal(predictor,'som'):
            model=Som()
        return model.fit(data)

    def predict(self,data):
        return self.model.predict(data)