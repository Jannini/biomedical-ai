from cProfile import label
from sklearn.svm  import SVR
from sklearn.linear_model import Lasso,ElasticNet,Ridge,LinearRegression
from sklearn.ensemble import RandomForestRegressor
#from biomedical_ai.inference.settings import DEFAULT_SETTINGS
from biomedical_ai.prediction.models.WeightedSubsetEnsemblePredictor import WeightedSubsetEnsembleClassifier
from catboost import CatBoostClassifier, CatBoostRegressor,Pool
import numpy as np
import math
import xgboost as xgb
from biomedical_ai.utils.utils import is_equal
class Regressor():
    def __init__(self,settings=None) -> None:
        self.settings=settings
        self.model=None
    def fit(self,data,labels,hyperparameters=None,settings=None):
        predictor=self.settings['PREDICTOR']
        hyperparams=self.settings['HYPERPARAMETER OPTIMIZATION']['METHOD']
        MODELS={
            'catboost':self._fit_catboost,
            'rforest':self._fit_rforest,
            'linsvm':self._fit_linsvm,
            'polysvm':self._fit_polysvm,
            'sigsvm':self._fit_sigsvm,
            'rbfsv,':self._fit_rbfsvm,
            'linreg':self._fit_linreg,
            'reglasso':self._fit_lasso,
            'regenet':self._fit_enet,
            'regridge':self._fit_ridge,
            'xgboost':self._fit_xgboost
        }
        fit_model=MODELS[predictor]
        fit_model(data,labels,**hyperparameters)

        return self.model

    def predict(self,data):
        return self.model.predict(data)
    
    def _fit_catboost(self,data,labels,**hyperparameters):
        self.model=CatBoostRegressor(**hyperparameters)
        self.model.fit(data,labels)
    def _fit_rforest(self,data,labels,**hyperparameters):
        self.model=RandomForestRegressor(random_state=0,**hyperparameters)
        self.model.fit(data,labels)
    def _fit_linsvm(self,data,labels,**hyperparameters):
        self.model=SVR(kernel='linear',**hyperparameters)
        self.model.fit(data,labels)
    def _fit_rbfsvm(self,data,labels,**hyperparameters):
        self.model=SVR(kernel='rbf',**hyperparameters)
        self.model.fit(data,labels)
    def _fit_sigsvm(self,data,labels,**hyperparameters):
        self.model=SVR(kernel='sigmoid',**hyperparameters)
        self.model.fit(data,labels)
    def _fit_polysvm(self,data,labels,**hyperparameters):
        self.model=SVR(kernel='polynomial',**hyperparameters)
        self.model.fit(data,labels)
    def _fit_linreg(self,data,labels,**hyperparameters):
        self.model=LinearRegression()
        self.model.fit(data,labels)
    def _fit_xgboost(self,data,labels,**hyperparameters):
        self.model=xgb()
        self.model.train(data,labels,**hyperparameters)
    def _fit_ridge(self,data,labels,**hyperparameters):
        self.model=Ridge(**hyperparameters)
        self.model.fit(data,labels)
    def _fit_enet(self,data,labels,**hyperparameters):
        self.model=ElasticNet(**hyperparameters)
        self.model.fit(data,labels)
    def _fit_lasso(self,data,labels,**hyperparameters):
        self.model=Lasso(**hyperparameters)
        self.model.fit(data,labels)