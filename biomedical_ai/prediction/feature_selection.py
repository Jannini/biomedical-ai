
from sklearn.feature_selection import RFECV
from sklearn.linear_model import LassoCV,ElasticNetCV
#import mrmr
from sklearn.svm import SVC
from biomedical_ai.prediction.models.EncoderPredictor import EncoderPredictor
from biomedical_ai.utils.utils import is_equal
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from biomedical_ai.prediction.models.PCAWrapper import PCAWrapper

class FeatureSelector():
    def __init__(self,settings=None) -> None:
        self.settings=settings
        self.model=None
        self.selected_features=[]
    def select_features(self,X_train,y_train,X_test):
        X_train=self.fit_transform(X_train,y_train,self.settings)
        X_test=self.transform(X_test,self.settings)
        return X_train,X_test
    def fit_transform(self,data,labels,settings=None):
        selection_method=self.settings['FEATURE SELECTION']['METHOD']
        innercv=self.settings['FEATURE SELECTION']['CV']
        #TODO scorer=''
        if is_equal(selection_method,'rfe-svm'):
            self.model=RFECV(cv=innercv,estimator=SVC(kernel='linear',random_state=0))
            self.model=self.model.fit(data,labels)
            args=self.model.support_
            data=data[:,args]
            self.selected_features.append(args)
        elif is_equal(selection_method,'lasso'):
            self.model=LassoCV(cv=innercv,random_state=0)
            self.model=self.model.fit(data,labels)
            args=abs(self.model.coef_)>0
            data=data[:,args]
            self.selected_features.append(args)
        elif is_equal(selection_method,'enet'):
            self.model=ElasticNetCV(cv=innercv,random_state=0,l1_ratio=[0.25,0.5,0.75,0.85,0.95,0.99])
            self.model=self.model.fit(data,labels)
            args=abs(self.model.coef_)>0
            data=data[:,args]
            self.selected_features.append(args)
        elif is_equal(selection_method,'rfe-rf'):
            self.model=RFECV(cv=innercv,estimator=RandomForestClassifier(random_state=0),scoring=scorer)
            self.model=self.model.fit(data,labels)
            args=self.model.support_
            data=data[:,args]
            self.selected_features.append(args)
        elif is_equal(selection_method,'mrmr'):
            try:
                self.model=MutualInformationForwardSelection(method='MRMR',k=5,categorical=True,n_jobs=1)
                self.model=self.model.fit(data,labels)
            except:
                self.model=MutualInformationForwardSelection(method='MRMR',num_bins=10,categorical=True,n_jobs=1)
                self.model=self.model.fit(data,labels)  
            args=self.model.get_support()
            data=data[:,args]
            self.selected_features.append(args)
        elif is_equal(selection_method,'pred-autoencoder'):
            self.model=EncoderPredictor(data,labels)
            self.model=self.model.fit_transform(data,labels)
        elif is_equal(selection_method,'pca'):
            self.model=PCAWrapper()
            data,num_components=self.model.fit_transform(data,0.95)
        return data
    def transform(self,data,settings=None):
        if self.selected_features:
            return data[:,self.selected_features[0]]
        else:
            return self.model.transform(data)