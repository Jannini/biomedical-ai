from sklearn.metrics import accuracy_score,balanced_accuracy_score,roc_auc_score,mean_absolute_error,mean_squared_error,recall_score,precision_score
#from biomedical_ai.inference.settings import DEFAULT_SETTINGS
from biomedical_ai.utils.utils import is_equal
import numpy as np

def _conf_mat(data,label,settings):
    pass
def _classification(true,pred,scores,settings):
    METRICS={
        'balanced_accuracy':balanced_accuracy_score,
        'accuracy':accuracy_score,
    }
    for metric in settings['METRICS']:
        calc_metric=METRICS[metric]
        score=calc_metric(true,pred).astype(np.float32)
        try:scores[metric].append(score)
        except:
            scores[metric]=[]
            scores[metric].append(score)
    return scores
def _regression(true,pred,scores,settings):
    METRICS={
        'mean_squared_error':mean_squared_error,
        'mean_absolute_error':mean_absolute_error,
    }
    for metric in settings['METRICS']:
        calc_metric=METRICS[metric]
        score=calc_metric(true,pred).astype(np.float32)
        try:scores[metric].append(score)
        except:
            scores[metric]=[]
            scores[metric].append(score)


def score(true,prediction,settings,scores=''):
    if scores=='':scores={}
    if is_equal(settings['PREDICTION TYPE'],'Classification'):
        scores=_classification(true,prediction,scores,settings)
    elif is_equal(settings['PREDICTION TYPE'],'Regression'):
        scores=_regression(true,prediction,scores,settings)
    return scores