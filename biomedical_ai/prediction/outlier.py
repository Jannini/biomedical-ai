from numpy.core.numeric import outer
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from biomedical_ai.utils.utils import is_equal
import numpy as np
from sklearn.preprocessing import MinMaxScaler
#import matplotlib.pyplot as plt

class Outlier():
    def __init__(self,settings=None) -> None:
        self.settings=settings
        self.model1=None
        self.model2=None
    def fit(self,data):
        self.data=data
        method='lof'
        #if is_equal(method,'forest'):
        self.model1=IsolationForest(random_state=0,)
        self.model1.fit(data)
        #elif is_equal(method,'lof'):
        self.model2=LocalOutlierFactor(novelty=True)
        self.model2.fit(data)
        #self.pca(data)
        return self.model1,self.model2
    def pca(self,data):
        from sklearn.decomposition import PCA 
        pca=PCA(random_state=0,n_components=2)
        pca=pca.fit(data)
        self.pca=pca
    def predict_single(self,data,model):
        data=np.reshape(data,(1,-1))
        outlier_score=model.decision_function(data)[0]
        outlier_scores=model.decision_function(self.data)
        median_score=np.max(outlier_scores)
        median_diff=abs(outlier_scores-median_score)
        mmscaler=MinMaxScaler()
        median_diff=mmscaler.fit_transform(np.reshape(median_diff,(-1,1)))
        outlier_score=mmscaler.transform(np.reshape([abs(outlier_score-median_score)],(-1,1)))[0][0]
        return outlier_score,median_diff
    def predict(self,data):
        outlier_score1,median_diff1=self.predict_single(data,self.model1)
        outlier_score2,median_diff2=self.predict_single(data,self.model2)
        outlier_score=(outlier_score1+outlier_score2)/2
        import plotly.graph_objects as go
        import matplotlib.pyplot as plt
        fig = go.Figure()
        fig.add_trace(go.Scatter(
            x=median_diff1[:,0], y=median_diff2[:,0],
                name='Training samples',
                mode='markers',
            marker_color='rgba(10, 30, 200, .5)',
                        marker=dict(
            size=5,)
        ))
        fig.add_trace(go.Scatter(
            x=[outlier_score1], y=[outlier_score2],
            name='Test sample',
                mode='markers',
            
            marker_color='red',
            marker=dict(
            size=12,)
        ))
        fig.update_traces(mode='markers')
        fig.update_layout(title='Outlier plot',
                  yaxis_title='LOF outlier score', xaxis_title='Isolation Forest outlier score')
        fig.update_xaxes(showgrid=False, zeroline=False)
        fig.update_yaxes(showgrid=False, zeroline=False)
        #plt.scatter(median_diff1,median_diff2)
        #plt.scatter(outlier_score1,outlier_score2)
        #plt.savefig('out1')
        return outlier_score,fig,outlier_score1,outlier_score2,np.percentile(median_diff1[:,0],90),np.percentile(median_diff2[:,0],90)