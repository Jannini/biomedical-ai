from sklearn.metrics import cohen_kappa_score,balanced_accuracy_score,roc_auc_score,accuracy_score,confusion_matrix
import numpy as np
class ComparePredictors():
    def __init__(self,pred1,pred2,true,data=None) -> None:
        self.pred1=np.asarray(pred1)
        self.pred2=np.asarray(pred2)
        self.true=np.asarray(true)
        self.data=data
    def calc_agreement(self):
        kappa=cohen_kappa_score(self.pred1,self.pred2)
        weighted_kappa=cohen_kappa_score(self.pred1,self.pred2,weights='linear')
        disagreement_conf_mat=confusion_matrix(self.pred1,self.pred2,normalize='all')
        disagreement=0
        for i in range(len(disagreement_conf_mat)):
            for j in range(len(disagreement_conf_mat[0])):
                if i!=j:
                    disagreement+=disagreement_conf_mat[i,j]
        self.kappa=kappa
        self.weighted_kappa=weighted_kappa
        self.disagreement=disagreement
        return kappa,weighted_kappa,disagreement
    
    def calc_stats(self,CV=1):
        conf_mat1=confusion_matrix(self.true,self.pred1)
        conf_mat2=confusion_matrix(self.true,self.pred2)
        Z,p_khat=self.KHAT_stats(conf_mat1,conf_mat2)
        scores1=[]
        scores2=[]
        T=np.nan
        p_t=np.nan
        if CV>1:
            CV_COUNT=round(len(self.true)/CV)
            for i in range(0,len(self.true)-CV_COUNT,CV_COUNT):  
                scores1.append(balanced_accuracy_score(self.true[i:i+CV_COUNT],self.pred1[i:i+CV_COUNT]))
                scores2.append(balanced_accuracy_score(self.true[i:i+CV_COUNT],self.pred2[i:i+CV_COUNT]))
            T,p_t=self.corrected_repeated_kfold_ttest(scores1,scores2,CV_COUNT*9,CV_COUNT)
        from mlxtend.evaluate import mcnemar_table,mcnemar
        mc_table=mcnemar_table(self.true,self.pred1,self.pred2)
        chi2, p_mc = mcnemar(ary=mc_table, corrected=True)
        print(1)


    def KHAT_stats(self,conf_mat_1,conf_mat_2,alpha=0.05):
        import math
        diagonal=(conf_mat_1[0,0]+conf_mat_1[1,1])
        s_row_1=np.sum(conf_mat_1[0,:])
        s_row_2=np.sum(conf_mat_1[1,:])
        s_column_1=np.sum(conf_mat_1[:,0])
        s_column_2=np.sum(conf_mat_1[:,1])
        samples=np.sum(conf_mat_1)
        kappa_1=samples*diagonal
        kappa_1=kappa_1-((s_row_1*s_column_1)+(s_row_2*s_column_2))
        denominator=samples**2-((s_row_1*s_column_1)+(s_row_2*s_column_2))
        kappa_1=kappa_1/denominator

        delta1=diagonal/samples
        delta2=((s_row_1*s_column_1)+(s_row_2*s_column_2))/(samples**2)
        delta3=(conf_mat_1[0,0]*(s_row_1+s_column_1))+(conf_mat_1[1,1]*(s_row_2+s_column_2))
        delta3=delta3/(samples*samples)
        delta4=0
        for i in range(2):
            for j in range(2):
                delta4=delta4+(conf_mat_1[i,j]*((np.sum(conf_mat_1[j,:])+np.sum(conf_mat_1[:,i]))**2))
        delta4=delta4/(samples**3)
        var_kappa_1=(delta1*(1-delta1))/((1-delta2)*(1-delta2))
        var_kappa_1=var_kappa_1+((2*(1-delta1)*(2*delta1*delta2-delta3))/((1-delta2)**3))
        var_kappa_1=var_kappa_1+(((1-delta1)*(1-delta1)*(delta4-4*(delta2**2)))/((1-delta2)**4))
        var_kappa_1=var_kappa_1/samples

        Z_1=kappa_1/math.sqrt(var_kappa_1)

        diagonal = (conf_mat_2[0, 0] + conf_mat_2[1, 1])
        s_row_1 = np.sum(conf_mat_2[0, :])
        s_row_2 = np.sum(conf_mat_2[1, :])
        s_column_1 = np.sum(conf_mat_2[:, 0])
        s_column_2 = np.sum(conf_mat_2[:, 1])
        samples = np.sum(conf_mat_2)

        kappa_2 = samples * diagonal
        kappa_2 = kappa_2 - ((s_row_1 * s_column_1) + (s_row_2 * s_column_2))
        denominator = samples**2 - ((s_row_1 * s_column_1) + (s_row_2 * s_column_2))
        kappa_2 = kappa_2 / denominator

        delta1=diagonal/samples
        delta2=((s_row_1*s_column_1)+(s_row_2*s_column_2))/(samples*samples)
        delta3=(conf_mat_2[0,0]*(s_row_1+s_column_1))+(conf_mat_2[1,1]*(s_row_2+s_column_2))
        delta3=delta3/(samples*samples)
        delta4=0
        for i in range(2):
            for j in range(2):
                delta4=delta4+(conf_mat_2[i,j]*((np.sum(conf_mat_2[j,:])+np.sum(conf_mat_2[:,i]))**2))
        delta4=delta4/(samples*samples*samples)
        var_kappa_2=(delta1*(1-delta1))/((1-delta2)**2)
        var_kappa_2=var_kappa_2+(((2*(1-delta1))*(2*delta1*delta2-delta3))/((1-delta2)**3))
        var_kappa_2=var_kappa_2+((((1-delta1)**2)*(delta4-4*(delta2**2)))/((1-delta2)**4))
        var_kappa_2=var_kappa_2/samples

        Z_2 = kappa_2 / math.sqrt(var_kappa_2)

        Z=abs(kappa_1-kappa_2)
        Z=Z/math.sqrt(abs(var_kappa_1+var_kappa_2))
        if alpha==0.05:
            Z_05=1.96

        if Z>Z_05:
            H=1
        else:
            H=0
        from scipy.stats import norm
        p=norm.cdf(-Z)*2
        return Z,p
    def corrected_repeated_kfold_ttest(self,data1, data2, n_training_samples, n_test_samples, alpha=0.05):
        from scipy.stats import t
        from math import sqrt
        n = len(data1)  

        mean_est = np.mean([(data1[i]-data2[i]) for i in range(n)])
        std_est = np.var([(data1[i]-data2[i]) for i in range(n)])
        test_training_ratio = n_test_samples / n_training_samples
        #test_training_ratio=0.1 
        denominator = sqrt((1 / n + test_training_ratio)*std_est)
        t_stat = mean_est / denominator
        # degrees of freedom
        df = n - 1
        # calculate the critical value
        cv = t.ppf(1.0 - alpha, df)
        # calculate the p-value
        p = (1.0 - t.cdf(abs(t_stat), df)) * 2.0
        # return everything
        return t_stat,  p

    def calc_scores(self):
        scores={
            'pred1':{'accuracy':None,'balanced_accuracy':None,'ROC_AUC':None},
            'pred2':{'accuracy':None,'balanced_accuracy':None,'ROC_AUC':None}
        }
        scores['pred1']['accuracy']=accuracy_score(self.true,self.pred1)
        scores['pred1']['balanced_accuracy']=balanced_accuracy_score(self.true,self.pred1)
        scores['pred1']['ROC_AUC']=roc_auc_score(self.true,self.pred1)

        scores['pred2']['accuracy']=accuracy_score(self.true,self.pred2)
        scores['pred2']['balanced_accuracy']=balanced_accuracy_score(self.true,self.pred2)
        scores['pred2']['ROC_AUC']=roc_auc_score(self.true,self.pred2)

        self.scores=scores
        return scores
    
    def create_plots(self):
        import matplotlib.pyplot as plt
        import seaborn as sns
        import pandas as pd
        from matplotlib.colors import ListedColormap
        fig, axes=plt.subplots(2,2,figsize=(12, 8), dpi=300)
        ax1=axes[0,0]
        ax2=axes[0,1]
        ax3=axes[1,0]
        ax4=axes[1,1]
        conf_mat1a=confusion_matrix(self.pred1[self.true==0],self.pred2[self.true==0],normalize='all')*100
        conf_mat2a=confusion_matrix(self.pred1[self.true==0],self.pred2[self.true==0])
        conf_mat1b=confusion_matrix(self.pred1[self.true==1],self.pred2[self.true==1],normalize='all')*100
        conf_mat2b=confusion_matrix(self.pred1[self.true==1],self.pred2[self.true==1])

        class_number=len(np.unique(self.true))
        ticks=np.linspace(0.5,0.5+class_number,class_number)
        labels=np.arange(0,class_number)
        df_cm = pd.DataFrame(conf_mat1a, range(class_number), range(class_number))
        df_cm2 = pd.DataFrame(conf_mat2a, range(class_number), range(class_number))
        sns.set(font_scale=1.3) # for label size
        sns.heatmap(df_cm2, ax=ax1,annot=True, annot_kws={'va':'bottom'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        sns.heatmap(df_cm,ax=ax1, annot=True, annot_kws={"va": 'top'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        df_cm = pd.DataFrame(conf_mat1b, range(class_number), range(class_number))
        df_cm2 = pd.DataFrame(conf_mat2b, range(class_number), range(class_number))
        sns.set(font_scale=1.3) # for label size
        sns.heatmap(df_cm2,ax=ax2, annot=True, annot_kws={'va':'bottom'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        sns.heatmap(df_cm, ax=ax2,annot=True, annot_kws={"va": 'top'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')

        conf_mat1a=confusion_matrix(self.pred1,self.true,normalize='pred')*100
        conf_mat2a=confusion_matrix(self.pred1,self.true)
        conf_mat1b=confusion_matrix(self.pred2,self.true,normalize='pred')*100
        conf_mat2b=confusion_matrix(self.pred2,self.true)

        df_cm = pd.DataFrame(conf_mat1a, range(class_number), range(class_number))
        df_cm2 = pd.DataFrame(conf_mat2a, range(class_number), range(class_number))
        sns.set(font_scale=1.3) # for label size
        sns.heatmap(df_cm2, ax=ax3,annot=True, annot_kws={'va':'bottom'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        sns.heatmap(df_cm,ax=ax3, annot=True, annot_kws={"va": 'top'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        df_cm = pd.DataFrame(conf_mat1b, range(class_number), range(class_number))
        df_cm2 = pd.DataFrame(conf_mat2b, range(class_number), range(class_number))
        sns.set(font_scale=1.3) # for label size
        sns.heatmap(df_cm2,ax=ax4, annot=True, annot_kws={'va':'bottom'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        sns.heatmap(df_cm, ax=ax4,annot=True, annot_kws={"va": 'top'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        #plt.title(f'')
        ax1.set(xlabel=self.data['pred2']['name'], ylabel=self.data['pred1']['name'],title='True score: 0 (good)')
        ax2.set(xlabel=self.data['pred2']['name'], ylabel=self.data['pred1']['name'],title='True score: 1 (bad)')
        ax3.set(xlabel='True score', ylabel=f'{self.data["pred1"]["name"]} prediction',title=f'{self.data["pred1"]["name"]} ({str(round(self.scores["pred1"]["balanced_accuracy"]*100,3))}%)')
        ax4.set(xlabel='True score', ylabel=f'{self.data["pred2"]["name"]} prediction',title=f'{self.data["pred2"]["name"]} ({str(round(self.scores["pred2"]["balanced_accuracy"]*100,3))}%)')
        plt.tight_layout()
        #self.data['pred1]['name']_save=str.replace(self.data['pred1]['name'],' ','_')
        #self.data['pred2']['name']_save=str.replace(self.data['pred2']['name'],' ','_')
        plt.savefig(f'./results_stats/{self.data["pred1"]["name"]}_{self.data["pred2"]["name"]}_detailed')
        plt.clf()
        kappa=self.kappa
        conf_mat1=confusion_matrix(self.pred1,self.pred2,normalize='all')*100
        conf_mat2=confusion_matrix(self.pred1,self.pred2)

        plt.figure( figsize=(8, 6), dpi=300)
        df_cm = pd.DataFrame(conf_mat1, range(class_number), range(class_number))
        df_cm2 = pd.DataFrame(conf_mat2, range(class_number), range(class_number))
        sns.set(font_scale=1.3) # for label size
        sns.heatmap(df_cm2, annot=True, annot_kws={'va':'bottom'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        sns.heatmap(df_cm, annot=True, annot_kws={"va": 'top'},cmap=ListedColormap(['grey']),cbar=False,fmt='g')
        
        plt.title(f'Cohen kappa: {round(kappa,3)}')
        plt.ylabel(self.data["pred1"]["name"])
        plt.xlabel(self.data["pred2"]["name"])
        plt.xticks(ticks,labels=labels)
        plt.yticks(ticks,labels=labels)
        plt.tight_layout()
        plt.savefig(f'./results_stats/{self.data["pred1"]["name"]}_{self.data["pred2"]["name"]}')

    def compare(self):
        self._calc_agreement()
        self._calc_scores()
        self._calc_significance()
        self._create_plots()
