from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
class PCAWrapper(PCA):
    def __init__(self,n_components=None) -> None:
        self.pca=PCA(n_components=n_components)
    def fit_transform(self,data,total_cumulative_variance=0.95):
        self.total_cumulative_variance=total_cumulative_variance
        self.pca=self.pca.fit(data)
        cumul_var=0
        for i in range(self.pca.n_components_):
            cumul_var+=self.pca.explained_variance_ratio_[i]
            if cumul_var>=total_cumulative_variance:
                #print(i)
                self.threshold=i
                data=self.pca.transform(data)
                data=data[:,:i+1]
                break
        return np.asarray(data),i
    def transform(self,data):
        data=self.pca.transform(data)
        data=data[:,:self.threshold+1,]
        return np.asarray(data)
    def save(self,folder,feature_names):
        components_variances=[]
        component_data=[]
        for i in range(len(self.pca.components_)):
            component_data.append([x for _, x in sorted(zip(abs(self.pca.components_[i]), feature_names), reverse=True)])
            component_data.append(sorted(abs(self.pca.components_[i]), reverse=True))
            try:
                components_variances.append(["Component "+str(i), str(self.pca.explained_variance_ratio_[i])])
            except:
                components_variances.append(["Component "+str(i), 'nan'])

        pca_dataf = pd.DataFrame(data=np.asarray(component_data).transpose(), columns=np.asarray(components_variances).flatten())
        return pca_dataf