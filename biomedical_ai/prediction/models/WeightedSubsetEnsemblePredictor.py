import xgboost as xgb
import numpy as np
from itertools import combinations
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.impute import SimpleImputer
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import balanced_accuracy_score
from scipy.stats import entropy
import shap
from sklearn.neighbors import NearestNeighbors
#import datapane as dp
class WeightedSubsetEnsembleClassifier:
    def __init__(self,model='xgboost',ensemble_weighting_model='logreg',verbose=False) -> None:
        self.model=model
        self.ensemble_weighting_model=ensemble_weighting_model
        self.verbose=verbose
    def fit(self,data,labels,feature_subsets={}):
        self.trained_models={}
        self.trained_model_cv_outputs={}
        self.ensemble_weighted_models={}  
        self.ensemble_weighted_model_scores={}
        self.ensemble_weighted_model_confidences={}
        self.ensemble_weighted_model_preds={}
        self.ensemble_weighted_model_true={}
        self.ensemble_neighbor_models={}
        self.test_data_true=np.asarray([])
        idx=0
        for feature_subset in feature_subsets.keys():
            if self.verbose: print(f"Fitting data on subset {feature_subset}")
            temp_data=np.asarray(data[:,feature_subsets[feature_subset][0]:feature_subsets[feature_subset][1]+1],dtype=np.float)
            if self.model=='auto':
                model_competition_scores=[]
                model_competition_models=[]
                model_competition_outputs=[]
                model_competition_names=['xgboost','linsvm','rbfsvm','rforest']
            #mask = np.all(np.isnan(temp_data), axis=1)
            #temp_data=temp_data[~mask]
            #temp_labels=labels[~mask]
            temp_labels=labels
            add_data=data[:,-1]
            #add_data=add_data[~mask]
            temp_data=np.hstack((temp_data,np.reshape(add_data,(-1,1))))
            if self.model=='xgboost' or self.model=='auto':
                if self.verbose: print("Fitting data using XGBoost")
                cw=list(compute_class_weight('balanced',classes=np.unique(temp_labels),y=temp_labels))
                w_array = np.ones(temp_labels.shape[0], dtype = 'float')
                for i, val in enumerate(temp_labels):
                    w_array[i] = cw[val]
                dtrain = xgb.DMatrix(temp_data, label=temp_labels,weight=w_array)
                parameters={}
                parameters["verbosity"]=0
                parameters["num_class"]=len(np.unique(temp_labels))
                parameters["evaluation_metric"]='merror'
                parameters["objective"]='multi:softprob'
                parameters['booster']="gbtree"
                parameters['nthread']=1
                temp_model = xgb.train(parameters, dtrain)
                if self.model=='auto':
                    model_competition_models.append(temp_model)
                else:
                    self.trained_models[feature_subset]=temp_model
                skf=StratifiedKFold(n_splits=5, random_state=0, shuffle=True)
                temp_scores=[]
                temp_outputs=[]
                for train_index, test_index in skf.split(temp_data,temp_labels):
                    X_train, X_test = temp_data[train_index], temp_data[test_index]
                    y_train, y_test = temp_labels[train_index], temp_labels[test_index]
                    w_array = np.ones(y_train.shape[0], dtype = 'float')
                    for i, val in enumerate(y_train):
                        w_array[i] = cw[val]
                    dtrain_temp = xgb.DMatrix(X_train, label=y_train)
                    dtest = xgb.DMatrix(X_test, label=y_test)
                    temp_cv_model = xgb.train(parameters, dtrain_temp)
                    preds = temp_cv_model.predict(dtest)
                    score=self._model_evaluate(preds,y_test)

                    if self.model!='auto':
                        try:
                            self.trained_model_cv_outputs[feature_subset]=np.vstack((self.trained_model_cv_outputs[feature_subset],preds))
                        except:
                            self.trained_model_cv_outputs[feature_subset]=preds
                        if idx==0:
                            try:
                                self.test_data_true=np.hstack((self.test_data_true,y_test))
                            except:
                                self.test_data_true=y_test
                    else:
                        try:
                            temp_outputs=np.vstack((temp_outputs,preds))
                        except:
                            temp_outputs=preds
                        temp_scores.append(score)
                        
                        if idx==0:
                            try:
                                self.test_data_true=np.hstack((self.test_data_true,y_test))
                            except:
                                self.test_data_true=y_test
                if self.model=='auto': 
                    model_competition_scores.append(np.mean(temp_scores))
                    model_competition_outputs.append(temp_outputs)

                idx+=1
            if self.model!='xgboost':
                
                if self.model=='auto':
                    models=['linsvm','rbfsvm','rforest']
                else: models=[self.model]
                for model in models:

                    if self.verbose: print(f"Fitting data using {model}")
                    if model=='linsvm':
                        temp_model=SVC(random_state=0,kernel='linear',probability=True,class_weight='balanced')
                    elif model=='rbfsvm':
                        temp_model=SVC(random_state=0,kernel='rbf',probability=True,class_weight='balanced')
                    elif model=='rforest':
                        temp_model=RandomForestClassifier(random_state=0,class_weight='balanced')
                    temp_model = temp_model.fit(temp_data,temp_labels)
                    if self.model=='auto':
                        model_competition_models.append(temp_model)
                    else:
                        self.trained_models[feature_subset]=temp_model

                    skf=StratifiedKFold(n_splits=5, random_state=0, shuffle=True)
                    temp_scores=[]
                    temp_outputs=np.asarray([])
                    for train_index, test_index in skf.split(temp_data,temp_labels):
                        X_train, X_test = temp_data[train_index], temp_data[test_index]
                        y_train, y_test = temp_labels[train_index], temp_labels[test_index]
                        if model=='linsvm':
                            temp_cv_model=SVC(random_state=0,kernel='linear',probability=True,class_weight='balanced')
                        elif model=='rbfsvm':
                            temp_cv_model=SVC(random_state=0,kernel='rbf',probability=True,class_weight='balanced')
                        elif model=='rforest':
                            temp_cv_model=RandomForestClassifier(random_state=0,class_weight='balanced')
                        temp_cv_model = temp_cv_model.fit(X_train,y_train)
                        preds = temp_cv_model.predict_proba(X_test)
                        score=self._model_evaluate(preds,y_test)
                        if self.model!='auto':
                            try:
                                self.trained_model_cv_outputs[feature_subset]=np.vstack((self.trained_model_cv_outputs[feature_subset],preds))
                            except:
                                self.trained_model_cv_outputs[feature_subset]=preds
                            if idx==0:
                                try:
                                    self.test_data_true=np.hstack((self.test_data_true,y_test))
                                except:
                                    self.test_data_true=y_test
                        else:
                            try:
                                temp_outputs=np.vstack((temp_outputs,preds))
                            except:
                                temp_outputs=preds
                                
                            temp_scores.append(score)
                    if self.model=='auto': 
                        model_competition_scores.append(np.mean(temp_scores))
                        model_competition_outputs.append(temp_outputs)
                    idx+=1
            if self.model=='auto':
                best_score=np.max(model_competition_scores)
                best_model=model_competition_names[np.argmax(model_competition_scores)]
                if self.verbose:print(f'Best model is {best_model} with {str(best_score)} accuracy')
                self.trained_model_cv_outputs[feature_subset]=model_competition_outputs[np.argmax(model_competition_scores)]
                self.trained_models[feature_subset]=model_competition_models[np.argmax(model_competition_scores)]
        trained_model_combinations=[]
        for i in range(1,len(list(feature_subsets.keys()))+1):
            trained_model_combinations.append(tuple(combinations(list(feature_subsets.keys()), i)))
        trained_model_combinations_flat=[]
        for i in trained_model_combinations:
            for j in i:
                trained_model_combinations_flat.append(j)
        if self.verbose: print(f"Fitting ensemble weights using {self.ensemble_weighting_model}")
        for ensemble in trained_model_combinations_flat:
            temp_data=np.array([])
            for trained_model_name in list(self.trained_models.keys()):
                if trained_model_name in ensemble:
                    try:
                        temp_data=np.hstack((temp_data,self.trained_model_cv_outputs[trained_model_name]))
                    except:
                        temp_data=self.trained_model_cv_outputs[trained_model_name]
            if self.ensemble_weighting_model=='logreg':
                temp_weighting_model=LogisticRegression(C=1,random_state=0)
                temp_weighting_model.fit(temp_data,self.test_data_true)
                temp_neighbor_model=NearestNeighbors(n_neighbors=50)
                temp_neighbor_model.fit(temp_data)
                skf=StratifiedKFold(n_splits=5, random_state=0, shuffle=True)
                temp_scores=[]
                temp_confidences=[]
                temp_outputs=np.asarray([])
                temp_labels=self.test_data_true
                for subset in ensemble:
                    try:
                        temp_outputs=np.hstack((temp_outputs,self.trained_model_cv_outputs[subset]))
                    except:
                        temp_outputs=self.trained_model_cv_outputs[subset]
                for train_index, test_index in skf.split(temp_outputs,temp_labels):
                    X_train, X_test = temp_outputs[train_index], temp_outputs[test_index]
                    y_train, y_test = temp_labels[train_index], temp_labels[test_index]
                    temp_weighting_cv_model=LogisticRegression(C=1,random_state=0)
                    temp_weighting_cv_model.fit(X_train,y_train)
                    pred=temp_weighting_cv_model.predict_proba(X_test)
                    for p in range(len(pred)) : 
                        try:
                            self.ensemble_weighted_model_preds[ensemble].append(pred[p])
                            self.ensemble_weighted_model_true[ensemble].append(y_test[p])
                        except:
                            self.ensemble_weighted_model_preds[ensemble]=[]
                            self.ensemble_weighted_model_preds[ensemble].append(pred[p])
                            self.ensemble_weighted_model_true[ensemble]=[]
                            self.ensemble_weighted_model_true[ensemble].append(y_test[p])
                    score=self._model_evaluate(pred,y_test)
                    confidence=self._calc_model_confidence(pred)
                    temp_scores.append(score)
                    temp_confidences.append(confidence)
            self.ensemble_weighted_model_confidences[ensemble]=np.mean(temp_confidences)
            self.ensemble_weighted_model_scores[ensemble]=np.mean(temp_scores)
            self.ensemble_weighted_models[ensemble]=temp_weighting_model
            self.ensemble_neighbor_models[ensemble]=temp_neighbor_model
        #print(1)
    def predict(self,data,feature_subsets={},medical_decision_support=False,imputer=None):
        imputer=SimpleImputer(strategy='constant',fill_value=0)
        predictions=[]
        patient_idx=0
        for sample in data:
            predictors_used=[]
            shap_values={}
            predictor_outputs=np.asarray([])
            for subset in feature_subsets:
                temp_data=np.asarray(sample[feature_subsets[subset][0]:feature_subsets[subset][1]+1],dtype=np.float)
                missing = np.isnan(temp_data)
                a,n_missing=np.unique(missing,return_counts=True)
                if True in missing and n_missing[np.argwhere(a==True)]>round(len(temp_data)*0.5):
                    continue
                add_data=sample[-1]
                temp_data=np.hstack((temp_data,add_data))
                try:
                    dtest = xgb.DMatrix(np.reshape(temp_data,(1,-1)))
                    pred=self.trained_models[subset].predict(dtest)
                    # TODO add training data
                    shap_values[subset]=self._calc_shap_val_sample(self.trained_models[subset],None,dtest)
                except:
                    dtest=imputer.fit_transform(np.reshape(temp_data,(1,-1)))
                    pred=self.trained_models[subset].predict_proba(dtest)
                predictors_used.append(subset)
                try:
                    predictor_outputs=np.hstack((predictor_outputs,pred))
                except:
                    predictor_outputs=pred
            ensemble_weighter=self.ensemble_weighted_models[tuple(predictors_used)]
            pred_ensemble=ensemble_weighter.predict_proba(predictor_outputs)
            predictions.append(pred_ensemble)
            potential_new_ensemble, new_data=self._calc_medical_support_new_data(predictors_used)
            similar_patients=self.ensemble_neighbor_models[tuple(predictors_used)]
            similar_patients=similar_patients.kneighbors(predictor_outputs,return_distance=False)
            similar_patients=similar_patients[0]
            score_gained=[]
            confidence_gained=[]
            score_gained_personal=[]
            confidence_gained_personal=[]
            for pnd in potential_new_ensemble:
                score_new=self._model_evaluate(np.asarray(self.ensemble_weighted_model_preds[tuple(pnd)])[similar_patients],np.asarray(self.ensemble_weighted_model_true[tuple(pnd)])[similar_patients])
                score_current=self._model_evaluate(np.asarray(self.ensemble_weighted_model_preds[tuple(predictors_used)])[similar_patients],np.asarray(self.ensemble_weighted_model_true[tuple(pnd)])[similar_patients])
                score_gained.append(self.ensemble_weighted_model_scores[tuple(pnd)]-self.ensemble_weighted_model_scores[tuple(predictors_used)])
                confidence_gained.append(self.ensemble_weighted_model_confidences[tuple(pnd)]-self.ensemble_weighted_model_confidences[tuple(predictors_used)])
                score_gained_personal.append(score_new-score_current)
            self._plot_patient_dashboard(pred_ensemble,new_data,score_gained,score_gained_personal,confidence_gained,predictor_outputs,predictors_used,patient_idx,shap_values)
            patient_idx+=1
        return predictions


    def _plot_patient_dashboard(self,pred_ensemble,new_data,score_gained,score_gained_personal,confidence_gained,outputs,ensemble,patient_idx,shapley_values):
        fig1=self._get_subset_predicton_plot(outputs,ensemble)
        fig2=self._plot_ensemble_prediction(pred_ensemble)
        fig3=self._plot_medical_decision_support(new_data,score_gained,score_gained_personal,confidence_gained)
        figs1=self._plot_shap_values(shapley_values)
        dp_figs=[]
        for fig in figs1: dp_figs.append(dp.Plot(fig))
        dp_figs=tuple(dp_figs)
        if len(dp_figs)==1: dp_group=dp.Group(dp_figs[0],columns=1)
        if len(dp_figs)==2: dp_group=dp.Group(dp_figs[0],dp_figs[1],columns=1)
        if len(dp_figs)==3: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],columns=1)
        if len(dp_figs)==4: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],dp_figs[3],columns=1)
        if len(dp_figs)==5: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],dp_figs[3],dp_figs[4],columns=1)
        if len(dp_figs)==6: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],dp_figs[3],dp_figs[4],dp_figs[5],columns=1)
        report=dp.Report(
        dp.Group(
            dp.Plot(fig1), 
            columns=1),
        dp.Group(
            dp.Plot(fig2), 
            dp.Plot(fig3),
            columns=2
        ),
        dp.Group(
            dp_group,
        )
        )
        report.save(path=f'./patient_dashboards/patient_{patient_idx}.html', open=False)
    def _plot_shap_values(self,shap_values):
        import plotly.graph_objects as go
        figs=[]
        for i in list(shap_values.keys()):
            shap_temp=shap_values[i]
            shap_sort_abs=[[abs(shap_temp[0][0])],[abs(shap_temp[1][0])],[abs(shap_temp[2][0])]]
            shap_sort_abs=np.sum(np.sum(shap_sort_abs,axis=1),axis=0)
            shap_temp_1= [x for _,x in sorted(zip(shap_sort_abs,shap_temp[0][0]),reverse=True)]
            shap_temp_2= [x for _,x in sorted(zip(shap_sort_abs,shap_temp[1][0]),reverse=True)]
            shap_temp_3= [x for _,x in sorted(zip(shap_sort_abs,shap_temp[2][0]),reverse=True)]
            fig = go.Figure(data=[
            go.Bar(name='Cognitive Normal', x=np.arange(len(shap_temp[0][0])), y=shap_temp_1),
            go.Bar(name='Mild Cognitive Impairment', x=np.arange(len(shap_temp[1][0])), y=shap_temp_2),
            go.Bar(name="Alzheimer's", x=np.arange(len(shap_temp[2][0])), y=shap_temp_3),

            ])
            fig.update_layout(title=f'{i.capitalize()} feature influences',barmode='group')
            fig.update_xaxes(title='Features')
            fig.update_yaxes(title='SHAP values')
            figs.append(fig)
        return figs
    def _model_evaluate(self,pred,true):
        predictions=[np.argmax(pred[p]) for p in range(len(pred)) ]
        bas=balanced_accuracy_score(predictions,true)*100
        return bas
    def _calc_model_confidence(self,pred):
        confidences=[]
        for i in pred:
            predicted=np.argmax(i)
            random_pred=1/3
            scaler=1.5
            confidence=(i[predicted]-random_pred)*scaler
            confidences.append(confidence*100)
        return np.mean(confidences)
    def _calc_shap_val_sample(self,predictor,train,test):
        explainer = shap.TreeExplainer(predictor)
        shap_values = explainer.shap_values(test)
        return shap_values
    def _plot_ensemble_prediction(self,pred_ensemble):
        import plotly.graph_objects as go
        fig = go.Figure(data=[
        go.Bar(name='Cognitive Normal', x=['Ensemble model'], y=np.asarray(pred_ensemble[0,0]*100)),
        go.Bar(name='Mild Cognitive Impairment', x=['Ensemble model'], y=np.asarray(pred_ensemble[0,1]*100)),
        go.Bar(name="Alzheimer's", x=['Ensemble model'], y=np.asarray(pred_ensemble[0,2]*100)),

        ])
        fig.update_layout(title='Weighted ensemble prediction',barmode='group')
        fig.update_xaxes(title='Modalities')
        fig.update_yaxes(title='Probability (%)')
        return fig
    def _plot_medical_decision_support(self,new_data,score_gained,score_gained_personal,confidence_gained):
        import plotly.graph_objects as go
        fig = go.Figure(data=[
        go.Bar(name='Accuracy increase', x=[x.capitalize() for x in new_data], y=score_gained),
        go.Bar(name='Accuracy increase (Personal)', x=[x.capitalize() for x in new_data], y=score_gained_personal),
        go.Bar(name='Confidence increase ', x=[x.capitalize() for x in new_data], y=confidence_gained),

        ])
        fig.update_layout(title='Next modality to include',barmode='group')
        fig.update_xaxes(title='Modalities')
        return fig
    def _calc_medical_support_new_data(self,ensemble):
        ensemble_length=len(ensemble)
        #average_score=self.ensemble_weighted_model_scores[ensemble]
        potential_new_ensemble=[]
        for i in self.ensemble_weighted_model_scores:
            if len(i)!=ensemble_length+1:
                continue
            difference=0
            
            for j in ensemble:
                if j not in i:
                    difference+=1
            if difference==0:
                potential_new_ensemble.append(i)
        new_data=[]
        for k in potential_new_ensemble:
            for l in k:
                if l not in ensemble:
                    new_data.append(l)
        return potential_new_ensemble,new_data
    def _get_subset_predicton_plot(self,outputs,ensemble):
        import plotly.graph_objects as go
        ad=[]
        mci=[]
        cn=[]
        for i in range(0,outputs.shape[1]-2,3):
            cn.append(outputs[0,i]*100)
            mci.append(outputs[0,i+1]*100)
            ad.append(outputs[0,i+2]*100)
        fig = go.Figure(data=[
        go.Bar(name='Cognitive Normal', x=[x.capitalize() for x in ensemble], y=cn),
        go.Bar(name='Mild Cognitive Impairment', x=[x.capitalize() for x in ensemble], y=mci),
        go.Bar(name="Alzheimer's", x=[x.capitalize() for x in ensemble], y=ad),

        ])
        fig.update_layout(title='Feature subset predictions',barmode='group')
        fig.update_yaxes(title='Probability (%)')
        #fig.write_html("./test01.html")
        return fig