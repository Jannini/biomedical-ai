import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
import tensorflow as tf

class EnsembleConnector():
    def __init__(self,models,output_size,layer_to_connect='output',model='logreg') -> None:
        if model=='logreg':
            pass
        elif model=='2lnn':
            INPUTS=[]
            CONNECTORS=[]
            LENGTH=0
            for m in models:
                m.trainable=False
                INPUTS.append(m.layers[0].input)
                CONNECTORS.append(m.layers[-1])
                LENGTH+=m.layers[-1].output_shape[1]
            self.models=models
            #self.model=tf.keras.Sequential([
            concat=layers.Concatenate(axis=1)([models[0].output,models[1].output])
            output=layers.Dense(input_shape=[LENGTH],units=output_size,activation='softmax')(concat)
            model = tf.keras.Model(INPUTS,output)
            self.model=model
            self.model.compile(loss='categorical_crossentropy',  metrics=['accuracy'] , optimizer='adam')
    def fit(self,data,labels):
        self.model.fit(data,labels)
    def predict(self,data):
        return self.model.predict(data)