import xgboost as xgb
import numpy as np
from itertools import combinations
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.impute import SimpleImputer
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import balanced_accuracy_score
from scipy.stats import entropy
import shap
from sklearn.neighbors import NearestNeighbors
import tensorflow.keras as keras
import tensorflow.keras.layers as layers
import datapane as dp
from biomedical_ai.prediction.models.BasicDense import BasicDense
from biomedical_ai.prediction.models.ensemble.EnsembleConnector import EnsembleConnector
from biomedical_ai.prediction.model_creator import create_run_models
import yaml
from yaml import CLoader as Loader
import os
from biomedical_ai.utils.utils import is_equal
from biomedical_ai.miscellaneous.find_models import get_best_model

class EnsemblePredictor:
    def __init__(self,verbose=False) -> None:
        self.verbose=verbose
        self.modalities=[]
        dirname = os.path.dirname(__file__)
        self.default_settings=None
        with open(f'{dirname}/default_settings.yaml','r') as f:
            self.default_settings=yaml.load(f,Loader=Loader)
    def partial_fit(self,data,labels,subject_ids=0,modality_name='',feature_names=None,settings=None,model=None):
        current_settings=self.default_settings
        supplement_data={'name':modality_name,'feature_names':feature_names,'data':data,'labels':labels,'subject_ids':subject_ids}
        if not isinstance(settings,None):
            for key in self.default_settings.keys():
                try:current_settings[key]=settings[key]
                except:print(f'Key {key} is not defined')
            
        if not isinstance(model,None):supplement_data['model'] = model
        model_infos=create_run_models(data,labels,settings,supplement_data)
        if is_equal(current_settings['PREDICTION TYPE'],'regression'):maximize=False
        else:maximize=True
        model_info=get_best_model(model_infos,score=current_settings['main_score'],maximize=maximize)
        self.modalities.append(model_info)
            
        
    def fit_combine(self,layer_to_combine='output',model='logreg'):
        def combine_modalitywise_data(modalities):
            combined_data=[]
            combined_labels=[]
            all_sub_ids=[]
            for modality in modalities:
                    all_sub_ids.append(set(modality.subject_ids))
            subject_ids_intersection=list(set.intersection(all_sub_ids))
            for modality in modalities:
                idxs=[]
                for idx,subid in enumerate(modality.subject_ids):
                    if subid in subject_ids_intersection:
                        idxs.append(idx)
                combined_data=np.hstack((combined_data,modality.data[idxs]))
                combined_labels=np.hstack((combined_labels,modality.labels[idxs]))
            return combined_data,combined_labels


        trained_model_combinations=[]
        modality_names=[]
        for modality in self.modalities:modality_names.append(modality.name)
        for i in range(1,len(modality_names)+1):
            trained_model_combinations.append(tuple(combinations(modality_names, i)))
        trained_model_combinations_flat=[]
        for i in trained_model_combinations:
            for j in i:
                trained_model_combinations_flat.append(j)
        if self.verbose: print(f"Fitting ensemble weights using {layer_to_combine} and model {model}")
        for ensemble in trained_model_combinations_flat:
            if len(ensemble)==1:continue
            modalitywise_models=[]
            modalitywise_cv_models=[]
            for modality in self.modalities:
                if modality.name in ensemble:
                    modalitywise_models.append(modality.model)
                    modalitywise_cv_models.append(modality.models)
            connector_model=EnsembleConnector(modalitywise_models,len(np.unique(modalitywise_models[0].labels)))
            connector_model_data,connector_model_labels=combine_modalitywise_data(modalitywise_models)
            connector_model.fit(connector_model_data,connector_model_labels)
            neighbors=round(len(connector_model_labels)*0.05)
            temp_neighbor_model=NearestNeighbors(n_neighbors=neighbors)
            temp_neighbor_model.fit(connector_model_data)
            skf=StratifiedKFold(n_splits=5, random_state=0, shuffle=True)
            temp_scores=[]
            temp_confidences=[]
            idx=0
            for train_index, test_index in skf.split(connector_model_data,connector_model_labels):
                    X_train, X_test = connector_model_data[train_index], connector_model_data[test_index]
                    y_train, y_test = connector_model_labels[train_index], connector_model_labels[test_index]
                    temp_connector_cv_model=EnsembleConnector(modalitywise_cv_models[:,idx],len(np.unique(modalitywise_models[0].labels)))
                    temp_connector_cv_model.fit(X_train,y_train)
                    pred=temp_connector_cv_model.predict(X_test)
                    idx+=1
                    for p in range(len(pred)) : 
                        try:
                            self.ensemble_weighted_model_preds[ensemble].append(pred[p])
                            self.ensemble_weighted_model_true[ensemble].append(y_test[p])
                        except:
                            self.ensemble_weighted_model_preds[ensemble]=[]
                            self.ensemble_weighted_model_preds[ensemble].append(pred[p])
                            self.ensemble_weighted_model_true[ensemble]=[]
                            self.ensemble_weighted_model_true[ensemble].append(y_test[p])
                    score=self._model_evaluate(pred,y_test)
                    confidence=self._calc_model_confidence(pred)
                    temp_scores.append(score)
                    temp_confidences.append(confidence)
            self.ensemble_weighted_model_confidences[ensemble]=np.mean(temp_confidences)
            self.ensemble_weighted_model_scores[ensemble]=np.mean(temp_scores)
            self.ensemble_weighted_models[ensemble]=temp_connector_cv_model
            self.ensemble_neighbor_models[ensemble]=temp_neighbor_model
        #print(1)
    def predict(self,data,feature_subsets={},medical_decision_support=False,imputer=None):
        imputer=SimpleImputer(strategy='constant',fill_value=0)
        predictions=[]
        patient_idx=0
        for sample in data:
            predictors_used=[]
            shap_values={}
            predictor_outputs=np.asarray([])
            for subset in feature_subsets:
                temp_data=np.asarray(sample[feature_subsets[subset][0]:feature_subsets[subset][1]+1],dtype=np.float)
                missing = np.isnan(temp_data)
                a,n_missing=np.unique(missing,return_counts=True)
                if True in missing and n_missing[np.argwhere(a==True)]>round(len(temp_data)*0.5):
                    continue
                add_data=sample[-1]
                temp_data=np.hstack((temp_data,add_data))
                try:
                    dtest = xgb.DMatrix(np.reshape(temp_data,(1,-1)))
                    pred=self.trained_models[subset].predict(dtest)
                    # TODO add training data
                    shap_values[subset]=self._calc_shap_val_sample(self.trained_models[subset],None,dtest)
                except:
                    dtest=imputer.fit_transform(np.reshape(temp_data,(1,-1)))
                    pred=self.trained_models[subset].predict_proba(dtest)
                predictors_used.append(subset)
                try:
                    predictor_outputs=np.hstack((predictor_outputs,pred))
                except:
                    predictor_outputs=pred
            ensemble_weighter=self.ensemble_weighted_models[tuple(predictors_used)]
            pred_ensemble=ensemble_weighter.predict_proba(predictor_outputs)
            predictions.append(pred_ensemble)
            potential_new_ensemble, new_data=self._calc_medical_support_new_data(predictors_used)
            similar_patients=self.ensemble_neighbor_models[tuple(predictors_used)]
            similar_patients=similar_patients.kneighbors(predictor_outputs,return_distance=False)
            similar_patients=similar_patients[0]
            score_gained=[]
            confidence_gained=[]
            score_gained_personal=[]
            confidence_gained_personal=[]
            for pnd in potential_new_ensemble:
                score_new=self._model_evaluate(np.asarray(self.ensemble_weighted_model_preds[tuple(pnd)])[similar_patients],np.asarray(self.ensemble_weighted_model_true[tuple(pnd)])[similar_patients])
                score_current=self._model_evaluate(np.asarray(self.ensemble_weighted_model_preds[tuple(predictors_used)])[similar_patients],np.asarray(self.ensemble_weighted_model_true[tuple(pnd)])[similar_patients])
                score_gained.append(self.ensemble_weighted_model_scores[tuple(pnd)]-self.ensemble_weighted_model_scores[tuple(predictors_used)])
                confidence_gained.append(self.ensemble_weighted_model_confidences[tuple(pnd)]-self.ensemble_weighted_model_confidences[tuple(predictors_used)])
                score_gained_personal.append(score_new-score_current)
            self._plot_patient_dashboard(pred_ensemble,new_data,score_gained,score_gained_personal,confidence_gained,predictor_outputs,predictors_used,patient_idx,shap_values)
            patient_idx+=1
        return predictions


    def _plot_patient_dashboard(self,pred_ensemble,new_data,score_gained,score_gained_personal,confidence_gained,outputs,ensemble,patient_idx,shapley_values):
        fig1=self._get_subset_predicton_plot(outputs,ensemble)
        fig2=self._plot_ensemble_prediction(pred_ensemble)
        fig3=self._plot_medical_decision_support(new_data,score_gained,score_gained_personal,confidence_gained)
        figs1=self._plot_shap_values(shapley_values)
        dp_figs=[]
        for fig in figs1: dp_figs.append(dp.Plot(fig))
        dp_figs=tuple(dp_figs)
        if len(dp_figs)==1: dp_group=dp.Group(dp_figs[0],columns=1)
        if len(dp_figs)==2: dp_group=dp.Group(dp_figs[0],dp_figs[1],columns=1)
        if len(dp_figs)==3: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],columns=1)
        if len(dp_figs)==4: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],dp_figs[3],columns=1)
        if len(dp_figs)==5: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],dp_figs[3],dp_figs[4],columns=1)
        if len(dp_figs)==6: dp_group=dp.Group(dp_figs[0],dp_figs[1],dp_figs[2],dp_figs[3],dp_figs[4],dp_figs[5],columns=1)
        report=dp.Report(
        dp.Group(
            dp.Plot(fig1), 
            columns=1),
        dp.Group(
            dp.Plot(fig2), 
            dp.Plot(fig3),
            columns=2
        ),
        dp.Group(
            dp_group,
        )
        )
        report.save(path=f'./patient_dashboards/patient_{patient_idx}.html', open=False)
    def _plot_shap_values(self,shap_values):
        import plotly.graph_objects as go
        figs=[]
        for i in list(shap_values.keys()):
            shap_temp=shap_values[i]
            shap_sort_abs=[[abs(shap_temp[0][0])],[abs(shap_temp[1][0])],[abs(shap_temp[2][0])]]
            shap_sort_abs=np.sum(np.sum(shap_sort_abs,axis=1),axis=0)
            shap_temp_1= [x for _,x in sorted(zip(shap_sort_abs,shap_temp[0][0]),reverse=True)]
            shap_temp_2= [x for _,x in sorted(zip(shap_sort_abs,shap_temp[1][0]),reverse=True)]
            shap_temp_3= [x for _,x in sorted(zip(shap_sort_abs,shap_temp[2][0]),reverse=True)]
            fig = go.Figure(data=[
            go.Bar(name='Cognitive Normal', x=np.arange(len(shap_temp[0][0])), y=shap_temp_1),
            go.Bar(name='Mild Cognitive Impairment', x=np.arange(len(shap_temp[1][0])), y=shap_temp_2),
            go.Bar(name="Alzheimer's", x=np.arange(len(shap_temp[2][0])), y=shap_temp_3),

            ])
            fig.update_layout(title=f'{i.capitalize()} feature influences',barmode='group')
            fig.update_xaxes(title='Features')
            fig.update_yaxes(title='SHAP values')
            figs.append(fig)
        return figs
    def _model_evaluate(self,pred,true):
        predictions=[np.argmax(pred[p]) for p in range(len(pred)) ]
        bas=balanced_accuracy_score(predictions,true)*100
        return bas
    def _calc_model_confidence(self,pred):
        confidences=[]
        for i in pred:
            predicted=np.argmax(i)
            random_pred=1/3
            scaler=1.5
            confidence=(i[predicted]-random_pred)*scaler
            confidences.append(confidence*100)
        return np.mean(confidences)
    def _calc_shap_val_sample(self,predictor,train,test):
        explainer = shap.TreeExplainer(predictor)
        shap_values = explainer.shap_values(test)
        return shap_values
    def _plot_ensemble_prediction(self,pred_ensemble):
        import plotly.graph_objects as go
        fig = go.Figure(data=[
        go.Bar(name='Cognitive Normal', x=['Ensemble model'], y=np.asarray(pred_ensemble[0,0]*100)),
        go.Bar(name='Mild Cognitive Impairment', x=['Ensemble model'], y=np.asarray(pred_ensemble[0,1]*100)),
        go.Bar(name="Alzheimer's", x=['Ensemble model'], y=np.asarray(pred_ensemble[0,2]*100)),

        ])
        fig.update_layout(title='Weighted ensemble prediction',barmode='group')
        fig.update_xaxes(title='Modalities')
        fig.update_yaxes(title='Probability (%)')
        return fig
    def _plot_medical_decision_support(self,new_data,score_gained,score_gained_personal,confidence_gained):
        import plotly.graph_objects as go
        fig = go.Figure(data=[
        go.Bar(name='Accuracy increase', x=[x.capitalize() for x in new_data], y=score_gained),
        go.Bar(name='Accuracy increase (Personal)', x=[x.capitalize() for x in new_data], y=score_gained_personal),
        go.Bar(name='Confidence increase ', x=[x.capitalize() for x in new_data], y=confidence_gained),

        ])
        fig.update_layout(title='Next modality to include',barmode='group')
        fig.update_xaxes(title='Modalities')
        return fig
    def _calc_medical_support_new_data(self,ensemble):
        ensemble_length=len(ensemble)
        #average_score=self.ensemble_weighted_model_scores[ensemble]
        potential_new_ensemble=[]
        for i in self.ensemble_weighted_model_scores:
            if len(i)!=ensemble_length+1:
                continue
            difference=0
            
            for j in ensemble:
                if j not in i:
                    difference+=1
            if difference==0:
                potential_new_ensemble.append(i)
        new_data=[]
        for k in potential_new_ensemble:
            for l in k:
                if l not in ensemble:
                    new_data.append(l)
        return potential_new_ensemble,new_data
    def _get_subset_predicton_plot(self,outputs,ensemble):
        import plotly.graph_objects as go
        ad=[]
        mci=[]
        cn=[]
        for i in range(0,outputs.shape[1]-2,3):
            cn.append(outputs[0,i]*100)
            mci.append(outputs[0,i+1]*100)
            ad.append(outputs[0,i+2]*100)
        fig = go.Figure(data=[
        go.Bar(name='Cognitive Normal', x=[x.capitalize() for x in ensemble], y=cn),
        go.Bar(name='Mild Cognitive Impairment', x=[x.capitalize() for x in ensemble], y=mci),
        go.Bar(name="Alzheimer's", x=[x.capitalize() for x in ensemble], y=ad),

        ])
        fig.update_layout(title='Feature subset predictions',barmode='group')
        fig.update_yaxes(title='Probability (%)')
        #fig.write_html("./test01.html")
        return fig