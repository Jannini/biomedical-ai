import tensorflow as tf
import tensorflow.keras.layers as layers

class EncoderPredictor():
    def __init__(self,data,labels,compression_levels=[2,4,8],encoder_activations=['relu','relu','relu'],decoder_activations=['relu','relu'],
    dropout_encode=[0.2,0.2],dropout_decode=[0.2,0.2],optimizer='adam',loss_weights=[1,1]) -> None:
        feature_size=data.shape[1]
        output_predictor_size=len(np.unique(labels))
        if output_predictor_size>9:
            output_predictor_size=1
        input=tf.keras.Input(shape=(feature_size,))
        encoder=tf.keras.Sequential([
            layers.Dense(units=round(feature_size/compression_levels[0]), activation=encoder_activations[0]),
            layers.Dropout(dropout_encode[0]),
            layers.Dense(round(feature_size/compression_levels[1]), activation=encoder_activations[1]),
            layers.Dropout(dropout_encode[1]),
            layers.Dense(round(feature_size/compression_levels[2]), activation=encoder_activations[2])])

        decoder=tf.keras.Sequential([
            layers.Dense(round(feature_size/compression_levels[1]), activation=encoder_activations[0]),
            layers.Dropout(dropout_decode[0]),
            layers.Dense(round(feature_size/compression_levels[0]), activation=decoder_activations[1]),
            layers.Dropout(dropout_decode[1]),
            layers.Dense(feature_size)])
        if output_predictor_size==1:
            predictor=tf.keras.Sequential([
                layers.Dense(output_predictor_size)])
        else:
            predictor=tf.keras.Sequential([
                layers.Dense(output_predictor_size,activation='sigmoid')])
        encoder=encoder(input)
        out_autoencoder=decoder(encoder)
        out_predictor=predictor(encoder)

        trained_model = tf.keras.Model(inputs=[input],
                        outputs=[out_autoencoder,out_predictor])
        model=tf.keras.Model(inputs=[trained_model.input],outputs=[trained_model.layers[1].output])
        trained_model.compile(optimizer=optimizer,
                loss=['mean_squared_error', 'categorical_crossentropy'],
                loss_weights=loss_weights)
        self.trained_model=trained_model
        self.model=model
    def fit(self,data,labels,batch_size=32,epochs=300,verbose=False):
        self.trained_model.fit(data,[data,labels],batch_size=batch_size,epochs=epochs,verbose=verbose)
    def predict(self,data):
        return self.model.predict(data)