import tensorflow.keras as keras
import tensorflow.keras.layers as layers

class BasicDense():
    def __init__(self,feature_size) -> None:
            self.batch_size=32
            feature_size=feature_size
            output_shape=3
            loss="categorical_crossentropy"
            input = keras.Input(feature_size)
            dense1=layers.Dense(input_shape=[feature_size],units=round(feature_size/2),activation='relu')(input)
            drop1=layers.Dropout(0.2)(dense1)
            dense2=layers.Dense(round(feature_size/2),activation='relu')(drop1)
            drop2=layers.Dropout(0.2)(dense2)
            dense3=layers.Dense(round(feature_size/4),activation='relu')(drop2)
            output=layers.Dense(output_shape,activation='softmax')(dense3)

            model2=keras.Model(inputs=[input],outputs=[dense3])
            model=keras.Model(inputs=[input],outputs=[output])
            model.compile(loss=loss,  metrics=['accuracy'] , optimizer='adam')
            self.model=model
            self.model2=model2
    def fit(self,data,labels,cw=None):
        self.model.fit(data.astype('float32'),labels,batch_size=self.batch_size,epochs=150,verbose=False,class_weight=cw)
    def predict(self,data):
        return self.model.predict(data)