import numpy as np
from sklearn.model_selection import StratifiedKFold,KFold
#from utils import RepeatedStratifiedGroupKFold
from sklearn.metrics import confusion_matrix,balanced_accuracy_score,accuracy_score,roc_auc_score,mean_squared_error
import xgboost as xgb
from sklearn.utils import compute_class_weight
import logging
from biomedical_ai.utils.utils import is_equal
from biomedical_ai.prediction.cross_validation import cross_validation_selector
#logger = logging.getLogger('my-logger')
#logger.propagate = False
class HyperparameterOptimizer():
    def __init__(self,settings):
        self.settings=settings
    def optimize(self,data,labels):
        import optuna
        import warnings
        warnings.filterwarnings("ignore")
        logger = logging.getLogger()

        logger.setLevel(logging.INFO)  # Setup the root logger.
        #logger.addHandler(logging.FileHandler("./results/"+self.settings['folder-name']+"/Optuna.log", mode="w"))

        optuna.logging.enable_propagation()  # Propagate logs to the root logger.
        optuna.logging.disable_default_handler()
        VALUES=[]
        MODELS=[]
        def objective(trial,data,labels,cv):
            modelname=self.settings['PREDICTOR']
            num_class=len(np.unique(labels))
            if is_equal(modelname,'xgboost'):
                
                if is_equal(self.settings['PREDICTION TYPE'],'regression'):
                    evaluation='rmse'
                    objectiv='reg:squarederror'
                else:
                    evaluation='merror'
                    objectiv='multi:softprob'
                param = {
                    "verbosity": 0,
                    
                    #"seed":0,
                    "eval_metric":evaluation,
                    "booster": "gbtree", #gbtree
                    "nthread":1,
                    "objective": objectiv,
                    "lambda": trial.suggest_loguniform("lambda", 1e-3, 100),
                    "max_depth" : trial.suggest_int("max_depth", 1, 11),
                    "eta" : trial.suggest_loguniform("eta", 1e-8, 1.0),
                    "subsample":trial.suggest_uniform("subsample",0.4,1),
                    "colsample_bytree":trial.suggest_uniform("colsample_bytree",0.4,1),
                    #"min_child_weight":trial.suggest_uniform("min_child_weight",0.01,10)
                    
                }
                if num_class>2 and is_equal(self.settings['PREDICTION TYPE'],'classification'):
                    param['num_class']=num_class
            elif is_equal(modelname,'fcdnn'):
                from tensorflow import keras
                from tensorflow.keras import layers
                batch_size=32
                feature_size=self.data.shape[1]
                output_shape=None
                loss=None
                if num_class>2 and num_class<11:
                    output_shape=3
                    loss="categorical_crossentropy"
                elif num_class==2:
                    output_shape=2
                    loss="categorical_crossentropy"
                else:
                    output_shape=1
                    loss='mean_squared_error'
                model = keras.Sequential()
                model.add(layers.Dense(input_shape=[feature_size],units=trial.suggest_int('unit1',round(feature_size/2),feature_size),activation='relu'))
                model.add(layers.Dropout(trial.suggest_uniform('drop1',0.1,0.5)))
                model.add(layers.Dense(trial.suggest_int('unit2',round(feature_size/4),feature_size),activation=trial.suggest_categorical('act1',['relu','swish'])))
                model.add(layers.Dropout(trial.suggest_uniform('drop2',0.1,0.5)))
                model.add(layers.Dense(trial.suggest_int('unit3',round(feature_size/6),feature_size),activation=trial.suggest_categorical('act2',['relu','swish'])))
                if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                    model.add(layers.Dense(output_shape,activation='softmax'))
                else:
                    model.add(layers.Dense(output_shape))
                model.compile(loss=loss,  metrics=['accuracy'] , optimizer='adam')
            else:
                if is_equal(modelname,'lda'):
                    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
                    model=LinearDiscriminantAnalysis(solver='lsqr',shrinkage=trial.suggest_uniform('shrinkage',0,1))
                elif is_equal(modelname,'linsvm'):
                    from sklearn.svm import SVC,SVR
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                        model=SVC(random_state=0,kernel='linear',C=trial.suggest_loguniform('C',0.001,1000),class_weight='balanced')
                    else:
                        model=SVR(kernel='linear',C=trial.suggest_loguniform('C',0.001,1000))
                elif is_equal(modelname,'logreg'):
                    from sklearn.linear_model import LogisticRegression
                    model=LogisticRegression(max_iter=5000,random_state=0,solver='lbfgs',penalty='l2',C=trial.suggest_loguniform('C',0.001,1000))
                elif is_equal(modelname,'sigsvm'):
                    from sklearn.svm import SVC,SVR
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                        model=SVC(random_state=0,kernel='sigmoid',C=trial.suggest_loguniform('C',0.001,1000),
                    coef0=trial.suggest_loguniform('coef0',0.001,1000),class_weight='balanced')
                    else:
                        model=SVR(kernel='sigmoid',C=trial.suggest_loguniform('C',0.001,1000),
                    coef0=trial.suggest_loguniform('coef0',0.001,1000))
                elif is_equal(modelname,'polysvm'):
                    from sklearn.svm import SVC,SVR
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                        model=SVC(random_state=0,kernel='linear',C=trial.suggest_loguniform('C',0.001,1000),
                    coef0=trial.suggest_loguniform('coef0',0.001,1000),degree=trial.suggest_int('degree',2,5),class_weight='balanced')
                    else:
                        model=SVR(kernel='linear',C=trial.suggest_loguniform('C',0.001,1000),
                    coef0=trial.suggest_loguniform('coef0',0.001,1000),degree=trial.suggest_int('degree',2,5))
                elif is_equal(modelname,'rbfsvm'):
                    from sklearn.svm import SVC,SVR
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                        model=SVC(random_state=0,kernel='linear',C=trial.suggest_loguniform('C',0.001,1000),
                    coef0=trial.suggest_loguniform('coef0',0.001,1000),class_weight='balanced')
                    else:
                        model=SVR(kernel='linear',C=trial.suggest_loguniform('C',0.001,1000),
                    coef0=trial.suggest_loguniform('coef0',0.001,1000))
                elif is_equal(modelname,'rforest'):
                    from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                        model=RandomForestClassifier(class_weight='balanced',random_state=0,n_estimators=trial.suggest_int('n_estimators',10,500),
                    max_depth=trial.suggest_int('max_depth',1,15),max_features=trial.suggest_uniform('max_features',0.01,0.5))
                    else:
                        model=RandomForestRegressor(random_state=0,n_estimators=trial.suggest_int('trees',10,500),
                    max_depth=trial.suggest_int('depth',1,15),max_features=trial.suggest_uniform('max_features',0.01,0.5))
                elif is_equal(modelname,'linreg'):
                    pass
                elif is_equal(modelname,'ridge'):
                    from sklearn.linear_model import Ridge
                    model=Ridge(random_state=0,alpha=trial.suggest_loguniform('alpha',0.001,1000))

            cv_scores=[]
            skf=cross_validation_selector(self.settings)
            for train_index, test_index in skf.split(data,labels,groups=None):
                #pruning_callback = optuna.integration.XGBoostPruningCallback(trial, "validation-mlogloss")
                X_train, X_test = data[train_index], data[test_index]
                y_train, y_test = labels[train_index], labels[test_index]
                if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                    cw=list(compute_class_weight('balanced',classes=np.unique(y_train),y=y_train))
                    w_array = np.ones(y_train.shape[0], dtype = 'float')
                    for i, val in enumerate(y_train):
                        w_array[i] = cw[val]

                if is_equal(modelname,'xgboost'):
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                        dtrain = xgb.DMatrix(X_train, label=y_train,weight=w_array)
                    else:dtrain = xgb.DMatrix(X_train, label=y_train)
                    dtest = xgb.DMatrix(X_test, label=y_test)
                    #bst = xgb.train(param, dtrain, evals=[(dtest, "validation")], callbacks=[pruning_callback])
                    model = xgb.train(param, dtrain,num_boost_round=trial.suggest_int("n_trees", 5, 200))
                    pred = model.predict(dtest)
                elif is_equal(modelname,'fcdnn'):
                    if is_equal(self.settings['PREDICTION TYPE'],'classification'):
                                    
                        cw=list(compute_class_weight('balanced',classes=np.unique(y_train),y=y_train))
                        cw={0:cw[0],1:cw[1],2:cw[2]}
                        from sklearn.preprocessing import LabelBinarizer
                        lbinarizer=LabelBinarizer()
                        y_train_2=lbinarizer.fit_transform(y_train)
                        model.fit(X_train,y_train_2,batch_size=batch_size,epochs=trial.suggest_int('epoch',100,500),verbose=False,class_weight=cw)
                        pred=model.predict(X_test)
                    else:
                        model.fit(X_train,y_train,batch_size=batch_size,epochs=trial.suggest_int('epoch',100,500),verbose=False)
                        pred=model.predict(X_test)
                else:
                    model=model.fit(X_train,y_train)
                    pred=model.predict(X_test)
                #pred_labels = np.rint(preds)
                if is_equal(self.settings['PREDICTION TYPE'],"regression"):
                    rmse=mean_squared_error(y_test,pred,squared=False)
                    cv_scores.append(rmse)
                else:
                    try:
                    # rocauc = roc_auc_score(y_test, preds,average='weighted',multi_class='ovo')
                        if is_equal(modelname,'xgboost') or is_equal(modelname,'fcdnn'):
                            predictions=[np.argmax(pred[p]) for p in range(len(pred)) ]
                            try:
                                if num_class==2: predictions=np.rint(pred)
                            except:pass
                            pred=predictions
                        bas=balanced_accuracy_score(y_test,pred)
                        cv_scores.append(bas)
                    except:
                        cv_scores.append(0)
            VALUES.append(np.mean(cv_scores))
            MODELS.append(model)
            return np.mean(cv_scores)
        if is_equal(self.settings['PREDICTION TYPE'],'regression'):
            evaluation='rmse'
        else:
            evaluation='bas'
        if evaluation=="rmse":
            study=optuna.create_study(direction="minimize",sampler=optuna.samplers.TPESampler(seed=0,multivariate=True))
        else:
            study=optuna.create_study(direction="maximize",sampler=optuna.samplers.TPESampler(seed=0,multivariate=True)) #CmaEsSampler #RandomSampler
        cv_num=self.settings['HYPERPARAMETER OPTIMIZATION']['CV']
        iterations=self.settings['HYPERPARAMETER OPTIMIZATION']['ITERATIONS']
        study.optimize(lambda trial:objective(trial,data,labels,cv_num),n_trials=iterations)
        try:
            plot_par_cor=optuna.visualization.plot_parallel_coordinate(study)
            plot_import=optuna.visualization.plot_param_importances(study)
            plot_history=optuna.visualization.plot_optimization_history(study)
            plots=[plot_par_cor,plot_import,plot_history]
        except:pass
        return study.best_params