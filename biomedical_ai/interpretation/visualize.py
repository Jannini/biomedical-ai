import os
import pickle
from model_dataclass import ModelInfo
from utils import filterfilter
from PostProcessVisualize import PostProcessVisualize
import sys



FILTERS={
'FILTERS_ALL': ['cv_10_5','individual'],
'FILTERS_ANY': ['Diagnosis','MMSE','WholeBrain'],
'FILTERS_NONE': ['mrmr','late','tabnet','ebm','simple','dx3','clin','cog','gen','pet','mri','csf','genomics'],
}

if __name__ == "__main__":


    try:
        cmdline_arg=sys.argv[1]
        FILTERS['FILTERS_ALL'].append(cmdline_arg)
        FILTERS['FILTERS_NONE'].remove(cmdline_arg)
    except:
        print('No argument passed. Using default filters.')
        #FILTERS['FILTERS_ALL'].append('clin')
        #FILTERS['FILTERS_NONE'].remove('clin')

    
    all_model_info=[]

    for subdir, dirs, files in os.walk('./results'):
        for d in dirs:
            if filterfilter(FILTERS,d):continue
            try:
                model_info_dict = pickle.load( open( f"./results/{d}/model_info.p", "rb" ) )
                model_info=ModelInfo(loadfromfile=model_info_dict)
                all_model_info.append(model_info)
                #if model_info.settings['ordinal-strategy']=='regression': print(d)
            except:
                print(f"Failed to load {d}")
    ppvis=PostProcessVisualize(all_model_info)
    if 'dx3' in FILTERS['FILTERS_ALL']:
        ppvis.mode='longitudinal'
    elif 'clin' in FILTERS['FILTERS_ALL'] or 'cog' in FILTERS['FILTERS_ALL'] or 'csf' in FILTERS['FILTERS_ALL'] or 'mri' in FILTERS['FILTERS_ALL'] or 'pet' in FILTERS['FILTERS_ALL'] or 'gen' in FILTERS['FILTERS_ALL']:
        ppvis.mode='modality'
    #ppvis.rerun_models()
    print(2)
    ppvis.draw_all_plots()