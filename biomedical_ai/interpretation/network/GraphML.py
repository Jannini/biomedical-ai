from pgmpy.estimators.StructureScore import StructureScore
import sklearn.covariance as covariance
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd
from pyvis.network import Network as NVIS
from networkx.algorithms.assortativity import average_degree_connectivity as adc
from networkx.algorithms.assortativity import average_neighbor_degree as a_n_d
from networkx.algorithms.centrality import degree_centrality as dc
from sklearn.preprocessing import KBinsDiscretizer, MinMaxScaler
from pgmpy.models import BayesianModel
from pgmpy.estimators import ParameterEstimator, BicScore,BDeuScore, HillClimbSearch,MaximumLikelihoodEstimator,MmhcEstimator, StructureScore
import pylab as plt
from pgmpy.inference.EliminationOrder import WeightedMinFill, MinFill
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import mean_absolute_error as mae
import xgboost as xgb

class LinRegScore(StructureScore):
    def __init__(self, data, **kwargs):
        super(LinRegScore, self).__init__(data, **kwargs)
    def local_score(self, variable, parents):
        skf=KFold(n_splits=10,shuffle=True,random_state=0)
        labels=self.data[variable].values
        data=self.data[parents].values
        scores=[]
        evaluation="rmse"
        objective="reg:squarederror"
        for train,test in skf.split(data,labels):
            train_X,test_X=data[train],data[test]
            train_y,test_y=labels[train],labels[test]
            dtrain = xgb.DMatrix(train_X, label=train_y)
            dtest = xgb.DMatrix(test_X, label=test_y)
            current_best_params={}
            current_best_params["verbosity"]=0
                #current_best_params["num_class"]=len(np.unique(labels))
            current_best_params["evaluation_metric"]=evaluation
            current_best_params["objective"]=objective
            current_best_params['booster']="gbtree"
            current_best_params['nthread']=1
            model = xgb.train(current_best_params, dtrain)
            pred = model.predict(dtest)
            #model=LinearRegression()
            #model.fit(train_X,train_y)
            #pred=model.predict(test_X)
            temp_score=mae(test_y,pred)
            scores.append(temp_score)
        score=np.mean(scores)
        return score
class GraphML():
    def __init__(self,x,y,feature_names=None,df=None) -> None:
        self.data=np.asarray(x)
        self.labels=np.asarray(y)
        self.feature_names=feature_names
        self.df=df
    def construct_graph_bayes(self,mmhc=False):
        #data=np.hstack((self.data,np.reshape(self.labels,(-1,1))))
        data=self.data
        bins=7
        kbd=KBinsDiscretizer(n_bins=bins,encode='ordinal',strategy='uniform')
        data_original=data
        data=kbd.fit_transform(data)
        data_name=['a','b','very low','low','average','high','very high']
        #data_name=['extremely low','low','moderately low','below average','average','above average','moderately high','high','extremely high']
        data=np.asarray(data,dtype=np.str)
        for i in np.arange(bins):
            data[data==str(float(i))]=data_name[i]

        # data=pd.DataFrame(data,columns=np.append(self.feature_names,'Diagnosis'))
        # data_original=pd.DataFrame(data_original,columns=np.append(self.feature_names,'Diagnosis'))
        # black_list_edges=[]
        # for f in self.feature_names:
        #     black_list_edges.append(('Diagnosis',f))
        #     black_list_edges.append((f,'APOE4'))
        black_list_edges=None
        data=pd.DataFrame(data,columns=self.feature_names)
        data_original=pd.DataFrame(data_original,columns=self.feature_names)

        if mmhc:
            mmhc = MmhcEstimator(data)
            skeleton = mmhc.mmpc()
            print(skeleton.edges())
        scoring_method=BicScore(data) #BicScore(data)
        hc = HillClimbSearch(data, scoring_method=scoring_method)
        #hc=MmhcEstimator(data)
        if mmhc:best_model = hc.estimate(white_list=skeleton.to_directed().edges())
        else:best_model = hc.estimate(black_list=black_list_edges)

        model = BayesianModel(best_model.edges)
        model.fit(data,estimator=MaximumLikelihoodEstimator)
            

        nt = NVIS("800px", "1500px",directed=True)
        for f in self.feature_names:
            try:
                parents=model.pred[f]._atlas
                if len(parents)>0:
                    bic=LinRegScore(data_original)
                    weight=bic.local_score(f,parents.keys())
            except:weight=0.33
            nt.add_node(f,label=f,title=str(weight),value=1/weight)

        for edge in best_model.edges:
            #bic=BicScore(data)
            bic=LinRegScore(data_original)
            weight=bic.local_score(edge[0],[edge[1]])
            #if 'Diagnosis' in edge:weight=1
            nt.add_edge(edge[0],edge[1],value=1/weight,title=weight)
        #nt.from_nx(nx.Graph(model))
        return nt,nt
        
        
        
    def construct_graph_partial_correlation(self):
            data=np.hstack((self.data,np.reshape(self.labels,(-1,1))))
            data=MinMaxScaler().fit_transform(data)
            # if len(data[0])*0.9<=len(data[1]):solver='lars'
            # else:solver='cd'
            solver='cd'

            glcv=covariance.GraphicalLassoCV(alphas=10,n_refinements=5,cv=5,max_iter=1000,mode=solver)
            glcv.fit(data)
            connectivities=glcv.precision_
            GRAPHS=nx.Graph()
            VISGRAPHS=NVIS(height="800px",width="1500px")
            G=GRAPHS
            VG=VISGRAPHS
            connectivity=connectivities
            self.feature_names=np.append(self.feature_names,'Diagnosis')
            for feature in self.feature_names:
                G.add_node(feature)
                VG.add_node(feature,feature)
            for i in range(len(connectivity)):
                for j in range(len(connectivity[0])):
                    if i==j:continue
                    if connectivity[i,j]<0.3:continue
                    G.add_edge(self.feature_names[i], self.feature_names[j], weight= connectivity[i,j])
                    VG.add_edge(self.feature_names[i], self.feature_names[j],title=str(connectivity[i,j]),value=connectivity[i,j])
            return G,VG
            # VG.show(f'./graph_partial_correlation.html')
            # print(adc(G,weight='weight'))
            # print(np.mean(list(a_n_d(G,weight='weight').values())))
            # print(np.mean(list(dc(G).values())))
