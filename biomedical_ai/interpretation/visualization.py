import streamlit as st
import pandas as pd
import pickle
from biomedical_ai.utils.model_dataclass import ModelInfo
import numpy as np
import plotly.graph_objects as go
import streamlit.components.v1 as components

def visualize_EDA(EDA):
    st.set_page_config( layout='wide')
    with st.beta_expander('Basic'):
        st.write(EDA.basic_data['pd_describe'])
        st.write(EDA.basic_data['pd_info'])
        st.write(EDA.basic_data['pd_memory'])
    with st.beta_expander('Distributions'):
        idx=0
        for distribution in EDA.distribution_figures:
            #with st.beta_expander(EDA.feature_names[idx]):
            st.image(distribution)
            #    idx+=1
    with st.beta_expander('Correlations'):
        for key in EDA.pearson.keys():
            st.plotly_chart(EDA.pearson[key],use_container_width=True)
        for key in EDA.spearman.keys():
            st.plotly_chart(EDA.spearman[key],use_container_width=True)
    with st.beta_expander('Dendrograms'):
        for key in EDA.dendrogram_plots.keys():
            st.plotly_chart(EDA.dendrogram_plots[key],use_container_width=True)
    with st.beta_expander('Hypothesis tests'):
        st.write(EDA.hypothesis_test_table)
    with st.beta_expander('PCA'):
        idx=0
        for key in EDA.pca_tables.keys():
            st.write(EDA.pca_tables_titles[idx])
            st.write(EDA.pca_tables[key])
            idx+=1
    with st.beta_expander('Factor Analysis'):
        idx=0
        for key in EDA.fa_tables.keys():
            st.write(EDA.fa_tables_titles[idx])
            st.write(EDA.fa_tables[key])
            idx+=1
    with st.beta_expander('Prediction Tree Interactions'):
        st.write(EDA.pred_tree_table)
    with st.beta_expander('Network Analysis Statistics - Bayesian'):
        st.write('TODO')
    with st.beta_expander('Network Analysis Statistics - GLasso'):
        st.write('TODO')
        
    # TODO: again reintroduce graph analysis
    # for graph in EDA.networks.keys():
    #     if 'plot' not in graph:continue
    #     with st.beta_expander(f'Graph Analysis - {graph}'):
    #         EDA.networks[graph].show(f'./testing/temporary_network.html')
    #         HtmlFile = open("./testing/temporary_network.html", 'r', encoding='utf-8')
    #         source_code = HtmlFile.read() 
    #         components.html(source_code,height=700)