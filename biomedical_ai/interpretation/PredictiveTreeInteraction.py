import numpy as np
from sklearn.tree import DecisionTreeClassifier,DecisionTreeRegressor
from biomedical_ai.prediction.cross_validation import cross_validation_selector
from sklearn.metrics import mean_squared_error,balanced_accuracy_score

class PredictiveTreeInteraction:
    def __init__(self,categorical=True,cv=5):
        self.categorical =categorical
        self.cv=cv
        if categorical:
            self.model=DecisionTreeClassifier(random_state=0,max_depth=4)
            self.model_single=DecisionTreeClassifier(random_state=0,max_depth=2)
        else:
            self.model=DecisionTreeRegressor(random_state=0,max_depth=4)
            self.model_single=DecisionTreeRegressor(random_state=0,max_depth=2)
    def fit(self,data,labels,feature_names=None):
        if self.categorical:
            skf_settings={'CV TYPE':'StratifiedKFold','CV':self.cv}
        else:
            skf_settings={'CV TYPE':'KFold','CV':self.cv}
            
        self.individual_scores={}
        self.interaction_scores={}
        try:
            if feature_names==None:
                feature_names=np.arange(len(data[1]))
        except:pass
        for i in range(len(feature_names)):
            skf=cross_validation_selector(skf_settings)
            scores=[]
            temp_data=data[:,i]
            for train,test in skf.split(temp_data,labels):
                X_train, X_test = temp_data[train], temp_data[test]
                y_train, y_test = labels[train], labels[test]
                temp_model=self.model_single
                temp_model.fit(np.reshape(X_train,(-1,1)),y_train)
                pred=temp_model.predict(np.reshape(X_test,(-1,1)))
                if self.categorical:scores.append(balanced_accuracy_score(y_test,pred))
                else:scores.append(mean_squared_error(y_test,pred))
            self.individual_scores[str(feature_names[i])]=np.mean(scores)
        for i in range(len(feature_names)):
            for j in range(len(data[1])):
                if j>=i:continue
                skf=cross_validation_selector(skf_settings)
                temp_data=data[:,[i,j]]
                scores=[]
                for train,test in skf.split(temp_data,labels):
                    X_train, X_test = temp_data[train], temp_data[test]
                    y_train, y_test = labels[train], labels[test]
                    temp_model=self.model
                    temp_model.fit(X_train,y_train)
                    pred=temp_model.predict(X_test)
                    if self.categorical:scores.append(balanced_accuracy_score(y_test,pred))
                    else:scores.append(mean_squared_error(y_test,pred))
                self.interaction_scores[f"{str(feature_names[i])}&{str(feature_names[j])}"]=np.mean(scores)
    def rank_interaction(self):
        self.interaction_rank_scores={}
        for i in self.interaction_scores.keys():
            feature1,feature2=i.split("&")
            max_feature_score=max([self.individual_scores[feature1],self.individual_scores[feature2]])
            self.interaction_rank_scores[i]=self.interaction_scores[i]-max_feature_score
        interaction_rank_scores=list(self.interaction_rank_scores.values())
        interaction_rank_names=list(self.interaction_rank_scores.keys())
        args=np.flip(np.argsort(interaction_rank_scores))
        interaction_rank_scores=np.asarray(interaction_rank_scores)[args]
        interaction_rank_names=np.asarray(interaction_rank_names)[args]
        return interaction_rank_scores*100,interaction_rank_names
