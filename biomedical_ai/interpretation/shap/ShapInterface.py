import shap
from biomedical_ai.utils.utils import is_equal
class ShapInterface():
    def __init__(self,model,data,settings,background_data=None,feature_names=None) -> None:
        self.model=model
        self.data=data
        self.background_data=background_data
        self.settings=settings
        self.feature_names=feature_names
    def compute_shap(self):
        modelname=self.settings['PREDICTOR']
        shap_values=[]
        if is_equal(modelname,'xgboost') or is_equal(modelname,'rforest'):
            explainer = shap.TreeExplainer(self.model,self.background_data,check_additivity=False)
            shap_values = explainer.shap_values(self.data,check_additivity=False)
        elif is_equal(modelname,'fcdnn'):
            explainer = shap.DeepExplainer(self.model,self.background_data)
            shap_values = explainer.shap_values(self.data,check_additivity=False)
        elif is_equal(modelname,'enet') or is_equal(modelname,'linsvm') or is_equal(modelname,'linreg') or is_equal(modelname,'logreg') or is_equal(modelname,'lda'):
            explainer=shap.LinearExplainer(self.model,self.background_data,n_samples=len(self.background_data))
            shap_values = explainer.shap_values(self.data)
        elif is_equal(modelname,'polysvm') or is_equal(modelname,'sigsvm') or is_equal(modelname,'rbfsvm'):
            explainer = shap.KernelExplainer(self.model.predict,shap.kmeans(self.background_data,k=20))
            shap_values = explainer.shap_values(self.data,n_samples=len(self.background_data))
        return shap_values