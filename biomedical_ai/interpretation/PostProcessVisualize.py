from numpy.lib.function_base import select
import streamlit as st
from utils import filterfilter
import numpy as np
import plotly.graph_objects as go
import pandas as pd
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.preprocessing import StandardScaler
import pickle
from model_dataclass import ModelInfo

class PostProcessVisualize():
    def __init__(self,models,mode='cross_sectional'):
        self.models=models
        self.mode=mode
    def performance_plot(self,scores,scores_fsel,scores_opt,scores_fsel_opt,title='Diagnosis',summary=False,streamlit=st):
        if 'lda' in scores['name']:
                sortby=['lda','linsvm','logreg','sigsvm','polysvm','rbfsvm','fcdnn','rforest','xgboost']
        else:
                sortby=['linreg','linsvm','ridge','sigsvm','polysvm','rbfsvm','fcdnn','rforest','xgboost']
        args1=self._sort_modelnames(scores['name'],sortby)
        args2=self._sort_modelnames(scores_fsel['name'],sortby)
        args4=self._sort_modelnames(scores_fsel_opt['name'],sortby)
        try: #Cleanup later
            args3=self._sort_modelnames(scores_opt['name'],sortby)
        except:
            sortby.remove('fcdnn')
            args3=self._sort_modelnames(scores_opt['name'],sortby)
        scores['name']=np.asarray(scores['name'])[args1]
        scores['scores']=np.asarray(scores['scores'])[args1]
        scores_fsel['name']=np.asarray(scores_fsel['name'])[args2]
        scores_fsel['scores']=np.asarray(scores_fsel['scores'])[args2]
        scores_fsel_opt['name']=np.asarray(scores_fsel_opt['name'])[args4]
        scores_fsel_opt['scores']=np.asarray(scores_fsel_opt['scores'])[args4]
        predictor_names1=np.asarray([[i]*10 for i in scores['name']]).flatten()
        predictor_names2=np.asarray([[i]*10 for i in scores_fsel['name']]).flatten()
        predictor_names3=np.asarray([[i]*10 for i in scores_opt['name']]).flatten()
        predictor_names4=np.asarray([[i]*10 for i in scores_fsel_opt['name']]).flatten()
        if summary:
            fig = go.Figure()
            fig.add_trace(go.Box(name='Accuracy',  y=np.asarray(scores['scores']).flatten()))
            fig.add_trace(go.Box(name='Accuracy w/ feature selection', y=np.asarray(scores_fsel['scores']).flatten()))
            fig.add_trace(go.Box(name='Accuracy w/ optimization',  y=np.asarray(scores_opt['scores']).flatten()))
            fig.add_trace(go.Box(name='Accuracy w/ feature selection & optimization', y=np.asarray(scores_fsel_opt['scores']).flatten()))

        else:

            fig = go.Figure()
            fig.add_trace(go.Box(name='Accuracy', x=predictor_names1, y=np.asarray(scores['scores']).flatten()))
            fig.add_trace(go.Box(name='Accuracy w/ feature selection', x=predictor_names2, y=np.asarray(scores_fsel['scores']).flatten()))
            fig.add_trace(go.Box(name='Accuracy w/ optimization', x=predictor_names3, y=np.asarray(scores_opt['scores']).flatten()))
            fig.add_trace(go.Box(name='Accuracy w/ feature selection & optimization', x=predictor_names4, y=np.asarray(scores_fsel_opt['scores']).flatten()))

# Change the bar mode
        fig.update_layout(title=title,boxmode='group',template='plotly_dark',width=1200)
        
        #fig.write_html('test1.html')
        streamlit.write(fig,use_container_width=True)
    def get_features_selected(self,info='accuracy',modelname=''):
        add=''
        if self.mode=='modality':
            add=str.split(modelname,'_')[3]
            add=add+'_'
        elif self.mode=='longitudinal':
            add='dx3_'
        if info == 'accuracy':
            model_info_dict = pickle.load( open( f"./results/adn_tp_Diagnosis_{add}linsvm_knn_cv_10_5_standardize_none_enetindividual/model_info.p", "rb" ) )
        elif info=='mmse_mae':
            model_info_dict = pickle.load( open( f"./results/adn_tp_MMSE_{add}linsvm_knn_cv_10_5_standardize_none_enetindividual/model_info.p", "rb" ) )
        elif info=='wb_mae':
            model_info_dict = pickle.load( open( f"./results/adn_tp_WholeBrain_{add}linsvm_knn_cv_10_5_standardize_none_enetindividual/model_info.p", "rb" ) )
        model_info=ModelInfo(loadfromfile=model_info_dict)
        features_selected_enet=np.zeros(len(model_info.feature_names))
        for i in model_info.selected_features:
            temp=i
            for t in range(len(temp)):
                if temp[t]:
                    features_selected_enet[t]+=1
        return features_selected_enet
    def get_individual_scores(self,features_to_use,info='accuracy',):
        add=''
        if self.mode=='longitudinal':
            add='dx3_'
        if info == 'accuracy':
            model_info_dict = pickle.load( open( f"./results/adn_tp_Diagnosis_{add}linsvm_simple_cv_10_5_standardize_none_noneindividual/model_info.p", "rb" ) )
        elif info=='mmse_mae':
            model_info_dict = pickle.load( open( f"./results/adn_tp_MMSE_{add}linsvm_simple_cv_10_5_standardize_none_noneindividual/model_info.p", "rb" ) )
        elif info=='wb_mae':
            model_info_dict = pickle.load( open( f"./results/adn_tp_WholeBrain_{add}linsvm_simple_cv_10_5_standardize_none_noneindividual/model_info.p", "rb" ) )
        model_info=ModelInfo(loadfromfile=model_info_dict)
        individual_scores=[]
        if info=='accuracy':
            idx=0
            for i in model_info.individual_scores['BAS']:
                if model_info.feature_names[idx] in features_to_use:
                    individual_scores.append(np.mean(np.asarray(i)*100))
                idx+=1
        elif info=='mmse_mae':
            idx=0
            for i in model_info.predictions:
                t=(np.asarray(np.abs(np.asarray(i)-model_info.data['y_test'][idx]))/30)*100
                vals=[]
                for a in t:
                    vals.append(np.mean(a))
                individual_scores.append(100-np.mean(vals))
                idx+=1
        elif info=='wb_mae':
            idx=0
            for i in model_info.predictions:
                t=(np.asarray(np.abs(np.asarray(i)-model_info.data['y_test'][idx]))/model_info.data['y_test'][idx])*100
                vals=[]
                for a in t:
                    vals.append(np.mean(a))
                individual_scores.append(100-np.mean(vals))
                idx+=1
        return individual_scores
    def get_shap_summary_values(self,model,modelname,info='accuracy',ret_all=False,xgb_only=False):
        feature_names=model.feature_names
        shap_values=model.shap_values_test
        if xgb_only:
            if modelname!='xgboost':
                a=np.zeros((len(feature_names)))
                a[a==0]=np.nan
                return a,a
        try:
            if not shap_values[0]:
                print(1)
                return np.zeros((len(feature_names))),np.zeros((len(feature_names)))
        except:pass
        shap_values_1=np.zeros((len(feature_names)))
        try:
            temp_shap=np.abs(np.mean(shap_values,axis=1))
        except:
            try:
                temp_shap=np.abs(shap_values)
            except:
                for i in range(10):
                    if modelname=='fcdnn' and info=='wb_mae':
                        shap_values[i]=np.abs(shap_values[i][0])
                    else:
                        shap_values[i]=np.abs(shap_values[i])
                temp_shap=shap_values
        if ret_all:
            if not model.selected_features:
                return temp_shap,temp_shap
            else:
                ret_shap=[]
                ret_shap2=[]
                for i in range(10):
                        feature_mask=np.argwhere(model.selected_features[i]==True)
                        fmask=[]
                        for f in feature_mask:
                            fmask.append(f[0])
                        idx=0
                        t_array=np.zeros((temp_shap[i].shape[0],len(feature_names)))
                        t_array2=t_array
                        t_array2[t_array2==0]=np.nan
                        t_array[:,fmask]=temp_shap[i]
                        t_array2[:,fmask]=temp_shap[i]
                        ret_shap.append(t_array)
                        ret_shap2.append(t_array2)
                return ret_shap,ret_shap2

                    
        try:    
                divider=np.zeros((len(feature_names)))
                for i in range(10):
                        feature_mask=np.argwhere(model.selected_features[i]==True)
                        fmask=[]
                        for f in feature_mask:
                            fmask.append(f[0])
                        divider[fmask]+=1
                        idx=0
                        for f in feature_mask:
                            tshap=np.mean(temp_shap[i],0)
                            shap_values_1[f[0]]+=tshap[idx]
                            idx+=1
                divider[divider==0]=1
                shap_values_1_no_fsel=shap_values_1/divider
                shap_values_1=shap_values_1/10
                
                    #shap=np.mean(shap_vals,0)
                    #shap=np.sum(shap,1)
        except:
                for i in range(10):
                        tshap=np.mean(temp_shap[i],0)
                        shap_values_1+=tshap
                shap_values_1=shap_values_1/10
                shap_values_1_no_fsel=shap_values_1

        return shap_values_1,shap_values_1_no_fsel
            
            
    def get_label_change_classes(self,orig_label,last_label):
        orig_label=np.asarray(orig_label)-1
        new_labels=[]
        for i in range(len(orig_label)):
            if orig_label[i]==last_label[i]:
                new_labels.append(0)
            elif orig_label[i]==0 and last_label[i]==1:
                new_labels.append(1)
            elif orig_label[i]==1 and last_label[i]==2:
                new_labels.append(2)
            elif orig_label[i]==0 and last_label[i]==2:
                new_labels.append(3)
            else:
                new_labels.append(0)
        return new_labels
        
    def conversion_performance_plot(self,model,orig_label,info='accuracy',last_label=None):
        orig_label=np.asarray(orig_label)-1
        try:model=model[0]
        except:pass
        preds=[]
        trues=[]
        for i in range(len(model.predictions)):
            for j in range(len(model.predictions[i])):
                    preds.append(model.predictions[i][j])
                    trues.append(model.data['y_test'][i][j])
        preds=np.asarray(preds)
        trues=np.asarray(trues)
        from sklearn.metrics import balanced_accuracy_score,accuracy_score,mean_absolute_error
        try:
            if info=='accuracy':
                pred_st=preds[orig_label==trues]
                true_st=trues[orig_label==trues]
                pred_conv=preds[orig_label<trues]
                true_conv=trues[orig_label<trues]
                pred_reg=preds[orig_label>trues]
                true_reg=trues[orig_label>trues]

                pred_st_1=pred_st[true_st==0]
                true_st_1=true_st[true_st==0]
                pred_st_2=pred_st[true_st==1]
                true_st_2=true_st[true_st==1]
                pred_st_3=pred_st[true_st==2]
                true_st_3=true_st[true_st==2]

                pred_conv_orig=orig_label[orig_label<trues]
                pred_conv_orig=pred_conv_orig[true_conv==2]
                pred_conv_1=pred_conv[true_conv==1]
                true_conv_1=true_conv[true_conv==1]
                pred_conv_2=pred_conv[true_conv==2]
                true_conv_2=true_conv[true_conv==2]
                pred_conv_3=pred_conv_2[pred_conv_orig==0]
                true_conv_3=true_conv_2[pred_conv_orig==0]
                pred_conv_2=pred_conv_2[pred_conv_orig==1]
                true_conv_2=true_conv_2[pred_conv_orig==1]
                
                acc_st1=100-(1-accuracy_score(true_st_1,pred_st_1))*100
                acc_st2=100-(1-accuracy_score(true_st_2,pred_st_2))*100
                acc_st3=100-(1-accuracy_score(true_st_3,pred_st_3))*100
                acc_cv1=100-(1-accuracy_score(true_conv_1,pred_conv_1))*100
                acc_cv2=100-(1-accuracy_score(true_conv_2,pred_conv_2))*100
                acc_cv3=100-(1-accuracy_score(true_conv_3,pred_conv_3))*100
                acc_reg=100-(1-accuracy_score(true_reg,pred_reg))*100
            elif info=='wb_mae' or info=='mmse_mae':
                last_label=np.asarray(last_label)-1
                pred_st=preds[orig_label==last_label]
                true_st=trues[orig_label==last_label]
                pred_conv=preds[orig_label<last_label]
                true_conv=trues[orig_label<last_label]
                pred_reg=preds[orig_label>last_label]
                true_reg=trues[orig_label>last_label]

                ll_st=last_label[orig_label==last_label]
                ll_cv=last_label[orig_label<last_label]

                pred_st_1=pred_st[ll_st==0]
                true_st_1=true_st[ll_st==0]
                pred_st_2=pred_st[ll_st==1]
                true_st_2=true_st[ll_st==1]
                pred_st_3=pred_st[ll_st==2]
                true_st_3=true_st[ll_st==2]

                pred_conv_orig=orig_label[orig_label<last_label]
                pred_conv_orig=pred_conv_orig[ll_cv==2]
                pred_conv_1=pred_conv[ll_cv==1]
                true_conv_1=true_conv[ll_cv==1]
                pred_conv_2=pred_conv[ll_cv==2]
                true_conv_2=true_conv[ll_cv==2]
                pred_conv_3=pred_conv_2[pred_conv_orig==0]
                true_conv_3=true_conv_2[pred_conv_orig==0]
                pred_conv_2=pred_conv_2[pred_conv_orig==1]
                true_conv_2=true_conv_2[pred_conv_orig==1]
                
                if info=="mmse_mae":
                    acc_st1=(np.abs(true_st_1-pred_st_1)/30)*100
                    acc_st2=(np.abs(true_st_2-pred_st_2)/30)*100
                    acc_st3=(np.abs(true_st_3-pred_st_3)/30)*100
                    acc_cv1=(np.abs(true_conv_1-pred_conv_1)/30)*100
                    acc_cv2=(np.abs(true_conv_2-pred_conv_2)/30)*100
                    acc_cv3=(np.abs(true_conv_3-pred_conv_3)/30)*100
                    acc_reg=(np.abs(true_reg-pred_reg)/30)*100
                elif info=='wb_mae':
                    acc_st1=(np.abs(true_st_1-pred_st_1)/true_st_1)*100
                    acc_st2=(np.abs(true_st_2-pred_st_2)/true_st_2)*100
                    acc_st3=(np.abs(true_st_3-pred_st_3)/true_st_3)*100
                    acc_cv1=(np.abs(true_conv_1-pred_conv_1)/true_conv_1)*100
                    acc_cv2=(np.abs(true_conv_2-pred_conv_2)/true_conv_2)*100
                    acc_cv3=(np.abs(true_conv_3-pred_conv_3)/true_conv_3)*100
                    acc_reg=(np.abs(true_reg-pred_reg)/true_reg)*100  
                acc_st1=100-np.mean(acc_st1)
                acc_st2=100-np.mean(acc_st2)
                acc_st3=100-np.mean(acc_st3)
                acc_cv1=100-np.mean(acc_cv1)
                acc_cv2=100-np.mean(acc_cv2)
                acc_cv3=100-np.mean(acc_cv3)
                acc_reg=100-np.mean(acc_reg)
        except:

            trues=np.argmax(trues,axis=1)
            pred_st=preds[orig_label==trues]
            true_st=trues[orig_label==trues]
            pred_conv=preds[orig_label<trues]
            true_conv=trues[orig_label<trues]
            pred_reg=preds[orig_label>trues]
            true_reg=trues[orig_label>trues]
            

            pred_st_1=pred_st[true_st==0]
            true_st_1=true_st[true_st==0]
            pred_st_2=pred_st[true_st==1]
            true_st_2=true_st[true_st==1]
            pred_st_3=pred_st[true_st==2]
            true_st_3=true_st[true_st==2]

            pred_conv_orig=orig_label[orig_label<trues]
            pred_conv_orig=pred_conv_orig[true_conv==2]
            pred_conv_1=pred_conv[true_conv==1]
            true_conv_1=true_conv[true_conv==1]
            pred_conv_2=pred_conv[true_conv==2]
            true_conv_2=true_conv[true_conv==2]
            pred_conv_3=pred_conv_2[pred_conv_orig==0]
            true_conv_3=true_conv_2[pred_conv_orig==0]
            pred_conv_2=pred_conv_2[pred_conv_orig==1]
            true_conv_2=true_conv_2[pred_conv_orig==1]
                
            acc_st1=100-(1-accuracy_score(true_st_1,pred_st_1))*100
            acc_st2=100-(1-accuracy_score(true_st_2,pred_st_2))*100
            acc_st3=100-(1-accuracy_score(true_st_3,pred_st_3))*100
            acc_cv1=100-(1-accuracy_score(true_conv_1,pred_conv_1))*100
            acc_cv2=100-(1-accuracy_score(true_conv_2,pred_conv_2))*100
            acc_cv3=100-(1-accuracy_score(true_conv_3,pred_conv_3))*100
            acc_reg=100-(1-accuracy_score(true_reg,pred_reg))*100
        size_st1=len(true_st_1)
        size_st2=len(true_st_2)
        size_st3=len(true_st_3)
        size_cv1=len(true_conv_1)
        size_cv2=len(true_conv_2)
        size_cv3=len(true_conv_3)
        size_reg=len(true_reg)
        return [acc_st1,acc_st2,acc_st3,acc_cv1,acc_cv2,acc_cv3,acc_reg],[size_st1,size_st2,size_st3,size_cv1,size_cv2,size_cv3,size_reg]
    def _axis1mean(self,array):
        means=[]
        for i in range(len(array)):
            means.append(np.mean(array[i]))
        return np.asarray(means)
    def _clamp(self,array,min=0,max=30,roundval=True):
        for i in range(len(array)):
            for j in range(len(array[i])):
                if array[i][j]<min:array[i][j]=min
                elif array[i][j]>max:array[i][j]=max
                if roundval:array[i][j]=round(array[i][j])
        return array
    def _sort_modelnames(self,modelnames,sortby):
        args=[]
        for sort in sortby:
            arg=list(modelnames).index(sort)
            args.append(arg)
        return args
    def _flatten(self,data):
        newarray=[]
        for i in data:
            for j in i:
                if type(j)==np.ndarray:
                    newarray.append(j[0])
                else:
                    newarray.append(j)
        return np.asarray(newarray)
    def _flatten_wrapper(self,model):
        model=model[0]
        return self._flatten(model.predictions)
    def retest_model(self,model,filename):
        try:model=model[0]
        except:pass
        model.predictions=[]
        df=pd.read_csv(f"./data/{filename}.csv")
        try:df=df.drop('RID',axis=1)
        except:pass
        try:df=df.drop('index',axis=1)
        except:pass
        try:df=df.drop('Unnamed: 0',axis=1)
        except:pass
        data=df.values
        labels=data[:,-1]/1000000
        data=data[:,:-1]
        #skf=StratifiedKFold(n_splits=10, random_state=0, shuffle=True)
        skf=KFold(n_splits=10, random_state=0, shuffle=True)
        sscaler=StandardScaler()
        from train_test_models import impute_missing
        idx=0
        import xgboost as xgb
        for train_index, test_index in skf.split(data,labels):
            X_train, X_test = data[train_index], data[test_index]
            y_train, y_test = labels[train_index], labels[test_index]
            X_train=sscaler.fit_transform(X_train)
            X_test=sscaler.transform(X_test)
            X_train,X_test=impute_missing(X_train,X_test,impute_strategy='knn')
            if 'enet' in model.settings['folder-name']:
                X_test=X_test[:,model.selected_features[idx]]

            temp_model=model.models[idx]
            dtest = xgb.DMatrix(X_test, label=y_test)
            preds=temp_model.predict(dtest)
            model.predictions.append(preds)
            idx+=1
        model.save()
    def time_passed_performance_plot(self,model,time_passed,info='accuracy'):
        time_passed=np.asarray(time_passed)/365.25/24/3600
        try:model=model[0]
        except:pass
        preds=[]
        trues=[]
        for i in range(len(model.predictions)):
            for j in range(len(model.predictions[i])):
                    preds.append(model.predictions[i][j])
                    trues.append(model.data['y_test'][i][j])
        preds=np.asarray(preds)
        trues=np.asarray(trues)
        from sklearn.metrics import balanced_accuracy_score,mean_absolute_error
        scores=[]
        sizes=[]
        for i in range(1,8):
            if i==7:
                temp_preds=preds[time_passed>i]
                temp_trues=trues[time_passed>i]
            else:
                temp_time_passed=np.asarray(time_passed[time_passed>i-1])
                temp_preds=preds[time_passed>i-1]
                temp_preds=np.asarray(temp_preds)[temp_time_passed<i]
                temp_trues=trues[time_passed>i-1]
                temp_trues=np.asarray(temp_trues)[temp_time_passed<i]
            try:
                if info=='accuracy':
                    acc=balanced_accuracy_score(temp_trues,temp_preds)
                elif info=='mmse_mae':
                    mae=mean_absolute_error(temp_trues,temp_preds)
                elif info=='wb_mae':
                    mae=mean_absolute_error(temp_trues,temp_preds)

            except:
                if info=='accuracy':
                    temp_trues=np.argmax(temp_trues,axis=1)
                    acc=balanced_accuracy_score(temp_trues,temp_preds)
            if info=='accuracy':
                score=100-(1-acc)*100
            elif info=='mmse_mae':
                score=100-(mae/30)*100
            elif info=='wb_mae':
                mae=np.abs(temp_trues-temp_preds)
                score=(mae/temp_trues)*100
                score=100-np.mean(score,axis=0)
            scores.append(score)
            sizes.append(len(temp_preds))
        return scores,sizes
        fig = go.Figure()
        fig.add_trace(go.Bar( x=np.arange(1,8), y=scores,text=sizes))
# Change the bar mode
        fig.update_layout(title='Time Passed',template='plotly_dark')
        st.write(fig)
    def plot_comp(self,scores,sizes,modelnames,title='Time Passed',streamlit=st):
        if 'lda' in modelnames:
            sortby=['lda','linsvm','logreg','sigsvm','polysvm','rbfsvm','fcdnn','rforest','xgboost']
        elif 'linreg' in modelnames:
            sortby=['linreg','linsvm','ridge','sigsvm','polysvm','rbfsvm','fcdnn','rforest','xgboost']
        else:
            sortby=[]
        if not sortby:pass
        else:
            args=self._sort_modelnames(modelnames,sortby)
            modelnames=np.asarray(modelnames)[args]
            scores=np.asarray(scores)[args]
        fig = go.Figure()
        for i in range(len(scores)):    
            fig.add_trace(go.Bar( name=modelnames[i], y=scores[i],text=sizes))
# Change the bar mode
        fig.update_layout(title=title,template='plotly_dark',width=1200)
        #fig.update_yaxes(
        #range=[min(min(scores))-5,max(max(scores))+5])
        streamlit.write(fig)
    def filtered_models(self,filter):
        filtered_models=[]
        for model in self.models:
            fname=model.settings['folder-name']
            if not filterfilter(filter,fname):
                filtered_models.append(model)
        return filtered_models
    def get_percent_errors(self,models,info='accuracy'):
        model_results={'name':[],'scores':[]}
        for model in models:
            name=model.settings['folder-name']
            if self.mode=='longitudinal' or self.mode=='modality':
                model_results['name'].append(str.split(name,'_')[4])
            else:
                model_results['name'].append(str.split(name,'_')[3])
            if info=='accuracy':
                bas=model.scores['BAS']
                score=100-(1-np.asarray(bas))*100
            elif info=='mmse_mae':
                mae=model.scores['MAE']
                try:
                    if type(model.predictions[0][0])==np.ndarray:
                        model_predictions_2=[]
                        for i in range(len(model.predictions)):
                            temp_preds=[]
                            for j in range(len(model.predictions[i])):
                                temp_preds.append(float(model.predictions[i][j][0]))
                            model_predictions_2.append(temp_preds)
                        model.predictions=np.asarray(model_predictions_2)
                    preds=np.asarray(model.predictions).squeeze()
                    trues=np.asarray(model.data['y_test'])
                    preds=self._clamp(preds)
                    mae=np.abs(trues-preds)
                    score=(mae/30)*100
                    score=100-self._axis1mean(score)
                except:score=np.asarray(mae)
            elif info=='wb_mae':
                mae=model.scores['MAE']
                trues=np.asarray(model.data['y_test'])
                try:
                    preds=np.asarray(model.predictions).squeeze()
                    mae=np.abs(trues-preds)
                    score=(mae/trues)*100
                    #score=100-np.mean(score,axis=1)
                    score=100-self._axis1mean(score)
                    score[score<90]=90
                except:
                    try:
                        preds=self._flatten(model.predictions)
                        trues=self._flatten(model.data['y_test'])
                        mae=np.abs(trues-preds)
                        score=(mae/trues)*100
                        score=100-np.mean(score,axis=0)
                        score[score<90]=90
                    except:score=np.asarray(model.scores['MAE'])
            model_results['scores'].append(score)
        return model_results
    def get_time_passed_test_vector(self,info='diagnosis',vector='time_passed'):
        time_passed=[]
        add=''
        if self.mode=='longitudinal':add='_dx3'
        if info=='diagnosis':
            data=pd.read_csv(f'./data/adn_tp_Diagnosis{add}.csv')

            labels=data['Diagnosis_last'].values
            data=data[vector].values
            skf=StratifiedKFold(n_splits=10, random_state=0, shuffle=True)
            for train_index, test_index in skf.split(data,labels):
                train_time_passed, test_time_passed = data[train_index], data[test_index]
                for t in test_time_passed:
                    time_passed.append(t)

        elif info=='mmse_mae':
            data=pd.read_csv(f'./data/adn_tp_MMSE{add}.csv')

            data=data[vector].values
            skf=KFold(n_splits=10, random_state=0, shuffle=True)
            for train_index, test_index in skf.split(data):
                train_time_passed, test_time_passed = data[train_index], data[test_index]
                for t in test_time_passed:
                    time_passed.append(t)    
        elif info=='wb_mae':
            data=pd.read_csv(f'./data/adn_tp_WholeBrain{add}.csv')

            data=data[vector].values
            skf=KFold(n_splits=10, random_state=0, shuffle=True)
            for train_index, test_index in skf.split(data):
                train_time_passed, test_time_passed = data[train_index], data[test_index]
                for t in test_time_passed:
                    time_passed.append(t) 
        elif info=="mmse":
            data=pd.read_csv(f'./data/adn_tp_help_MMSE{add}.csv')

            data=data[vector].values
            skf=KFold(n_splits=10, random_state=0, shuffle=True)
            for train_index, test_index in skf.split(data):
                train_time_passed, test_time_passed = data[train_index], data[test_index]
                for t in test_time_passed:
                    time_passed.append(t)  
        elif info=='wb':
            data=pd.read_csv(f'./data/adn_tp_help_WholeBrain{add}.csv')

            data=data[vector].values
            skf=KFold(n_splits=10, random_state=0, shuffle=True)
            for train_index, test_index in skf.split(data):
                train_time_passed, test_time_passed = data[train_index], data[test_index]
                for t in test_time_passed:
                    time_passed.append(t) 
        return time_passed
    def predictor_correlation(self,modelnames,predictions,predictions2=[],streamlit=st):
        if 'lda' in modelnames:
            sortby=['lda','linsvm','logreg','sigsvm','polysvm','rbfsvm','fcdnn','rforest','xgboost']
        else:
            sortby=['linreg','linsvm','ridge','sigsvm','polysvm','rbfsvm','fcdnn','rforest','xgboost']
        args=self._sort_modelnames(modelnames,sortby)
        modelnames=np.asarray(modelnames)[args]
        predictions=np.asarray(predictions)[args]
        try:predictions2=np.asarray(predictions2)[args]
        except:pass
        
        from scipy.stats import pearsonr,spearmanr
        from sklearn.metrics import cohen_kappa_score
        pearson_correlation_matrix=np.ones((len(modelnames),len(modelnames)))
        spearman_correlation_matrix=np.ones((len(modelnames),len(modelnames)))
        for i in range(len(modelnames)):
            for j in range(len(modelnames)):
                if 'lda' in modelnames:
                    pearson_correlation_matrix[i,j]=cohen_kappa_score(predictions[i],predictions[j])
                    #pearson_correlation_matrix[i,j]=pearsonr(predictions[i],predictions[j])[0]
                    spearman_correlation_matrix[i,j]=spearmanr(predictions[i],predictions[j])[0]
                else:
                    pearson_correlation_matrix[i,j]=pearsonr(predictions[i],predictions[j])[0]
                    spearman_correlation_matrix[i,j]=spearmanr(predictions[i],predictions[j])[0]                    
        fig_pearson = go.Figure(data=go.Heatmap(z=pearson_correlation_matrix,x=modelnames,y=modelnames,hoverongaps = False))
        fig_spearman = go.Figure(data=go.Heatmap(z=spearman_correlation_matrix,x=modelnames,y=modelnames,hoverongaps = False))
        streamlit.write(fig_pearson,use_container_width=True)
        streamlit.write(fig_spearman,use_container_width=True)
    def get_filters(self,fname):
        FILTERS1={'FILTERS_ALL': [fname,'enet'],'FILTERS_ANY': [],'FILTERS_NONE': ['opt']}
        FILTERS2={'FILTERS_ALL': [fname,'enet','opt'],'FILTERS_ANY': [],'FILTERS_NONE': []}
        FILTERS3={'FILTERS_ALL': [fname,'opt'],'FILTERS_ANY': [],'FILTERS_NONE': ['enet']}
        FILTERS4={'FILTERS_ALL': [fname],'FILTERS_ANY': [],'FILTERS_NONE': ['enet','opt']}
        return FILTERS1, FILTERS2, FILTERS3, FILTERS4
    def rerun_models(self):
        add=''
        if self.mode=='longitudinal':add='_dx3'
        d='WholeBrain'
        FILTERS={'FILTERS_ALL': [d,'xgboost'],'FILTERS_ANY': [],'FILTERS_NONE': ['enet','opt']}
        self.retest_model(self.filtered_models(FILTERS),f'adn_tp_{d}{add}')
        FILTERS={'FILTERS_ALL': [d,'xgboost','enet'],'FILTERS_ANY': [],'FILTERS_NONE': ['opt']}
        self.retest_model(self.filtered_models(FILTERS),f'adn_tp_{d}{add}')
        FILTERS={'FILTERS_ALL': [d,'xgboost','enet','opt'],'FILTERS_ANY': [],'FILTERS_NONE': []}
        self.retest_model(self.filtered_models(FILTERS),f'adn_tp_{d}{add}')
        FILTERS={'FILTERS_ALL': [d,'xgboost','opt'],'FILTERS_ANY': [],'FILTERS_NONE': ['enet']}
        self.retest_model(self.filtered_models(FILTERS),f'adn_tp_{d}{add}')
    def find_best_models(self,filter):
        models=self.filtered_models(filter)

        data={}
        if 'Diagnosis' in filter['FILTERS_ALL']:
            scores=[]
            for m in models:
                scores.append(np.mean(m.scores['BAS']))
            arg=np.flip(np.argsort(scores))
        elif 'MMSE' in filter['FILTERS_ALL']:
            scores=[]
            for m in models:
                scores.append(np.mean(m.scores['MAE']))
            arg=np.argsort(scores)
        elif 'WholeBrain' in filter['FILTERS_ALL']:
            scores=[]
            for m in models:
                scores.append(np.mean(m.scores['MAE']))
            arg=np.argsort(scores)
        
        models=np.asarray(models)[arg]
        for m in models:
            name=m.settings['folder-name']
            if 'dx' in name or 'cog' in name or 'clin' in name or 'pet' in name or 'mri' in name or 'csf' in name or 'gen' in name:
                mname=str.split(name,'_')[4]
            else:
                mname=str.split(name,'_')[3]
            if mname not in data:
                data[mname]=m
        return data
    def draw_all_plots(self):
        #self.rerun_models()
        #exit()
        st.set_page_config( layout='wide',page_title=self.mode)
        f1,f2,f3,f4=self.get_filters('Diagnosis')
        diag_enet=self.get_percent_errors(self.filtered_models(f1),info='accuracy')
        diag_enetopt=self.get_percent_errors(self.filtered_models(f2),info='accuracy')
        diag_opt=self.get_percent_errors(self.filtered_models(f3),info='accuracy')
        diag=self.get_percent_errors(self.filtered_models(f4),info='accuracy')
        diagnosis = st.beta_expander(label='Diagnosis - Performance')
        mmsest = st.beta_expander(label='MMSE - Performance')
        wbst = st.beta_expander(label='WholeBrain - Performance')
        diagnosis2 = st.beta_expander(label='Diagnosis - Interpretation')
        mmsest2 = st.beta_expander(label='MMSE - Interpretation')
        wbst2 = st.beta_expander(label='WholeBrain - Interpretation')
        diagnosis=[diagnosis,diagnosis2]
        mmsest=[mmsest,mmsest2]
        wbst=[wbst,wbst2]
        self.performance_plot(diag,diag_enet,diag_opt,diag_enetopt,streamlit=diagnosis[0])
        self.performance_plot(diag,diag_enet,diag_opt,diag_enetopt,summary=True,streamlit=diagnosis[0])
        try:
            f1,f2,f3,f4=self.get_filters('MMSE')
            mmse_enet=self.get_percent_errors(self.filtered_models(f1),info='mmse_mae')
            mmse_enetopt=self.get_percent_errors(self.filtered_models(f2),info='mmse_mae')
            mmse_opt=self.get_percent_errors(self.filtered_models(f3),info='mmse_mae')
            mmse=self.get_percent_errors(self.filtered_models(f4),info='mmse_mae')
            self.performance_plot(mmse,mmse_enet,mmse_opt,mmse_enetopt,title='MMSE',streamlit=mmsest[0])
            self.performance_plot(mmse,mmse_enet,mmse_opt,mmse_enetopt,title='MMSE',summary=True,streamlit=mmsest[0])

            f1,f2,f3,f4=self.get_filters('WholeBrain')
            wb_enet=self.get_percent_errors(self.filtered_models(f1),info='wb_mae')
            wb_enetopt=self.get_percent_errors(self.filtered_models(f2),info='wb_mae')
            wb_opt=self.get_percent_errors(self.filtered_models(f3),info='wb_mae')
            wb=self.get_percent_errors(self.filtered_models(f4),info='wb_mae')
            self.performance_plot(wb,wb_enet,wb_opt,wb_enetopt,title='WholeBrain',streamlit=wbst[0])
            self.performance_plot(wb,wb_enet,wb_opt,wb_enetopt,title='WholeBrain',summary=True,streamlit=wbst[0])
        except:pass
        time_passed=self.get_time_passed_test_vector()
        orig_label=self.get_time_passed_test_vector(vector='Diagnosis')
        last_label=self.get_time_passed_test_vector(vector='Diagnosis_last')
        try:
            FILTER={'FILTERS_ALL': ['Diagnosis','xgboost','opt'],'FILTERS_ANY': [],'FILTERS_NONE': ['enet']}
            xgbmodel=self.filtered_models(FILTER)[0]
            shap_xgb=self.get_shap_summary_values(self.filtered_models(FILTER)[0],'xgboost',ret_all=True)
            shap_xgb=shap_xgb[0]
            shap_xgb=np.vstack((shap_xgb[0],shap_xgb[1],shap_xgb[2],shap_xgb[3],shap_xgb[4],shap_xgb[5],
            shap_xgb[6],shap_xgb[7],shap_xgb[8],shap_xgb[9]))
            self.cluster_shap(shap_xgb,xgbmodel.feature_names,np.asarray(orig_label)-1,xgbmodel.predictions,streamlit=diagnosis2)
        except:pass
 

        FILTER_Diagnosis={'FILTERS_ALL': ['Diagnosis'],'FILTERS_ANY': [],'FILTERS_NONE': []}
        best_models_diagnosis=self.find_best_models(FILTER_Diagnosis)
        self.draw_plots(best_models_diagnosis,time_passed,orig_label,streamlit=diagnosis,last_label=last_label)
        try:
            FILTER_MMSE={'FILTERS_ALL': ['MMSE'],'FILTERS_ANY': [],'FILTERS_NONE': []}
            best_models_mmse=self.find_best_models(FILTER_MMSE)
            time_passed=self.get_time_passed_test_vector(info='mmse_mae')
            orig_label=self.get_time_passed_test_vector(info='mmse_mae',vector='Diagnosis')
            last_label=self.get_time_passed_test_vector(info='mmse',vector='Diagnosis_last')
            self.draw_plots(best_models_mmse,time_passed,orig_label,info='mmse_mae',last_label=last_label,streamlit=mmsest)

            FILTER_WB={'FILTERS_ALL': ['WholeBrain'],'FILTERS_ANY': [],'FILTERS_NONE': []}
            best_models_wb=self.find_best_models(FILTER_WB)
            time_passed=self.get_time_passed_test_vector(info='wb_mae')
            orig_label=self.get_time_passed_test_vector(info='wb_mae',vector='Diagnosis')
            last_label=self.get_time_passed_test_vector(info='wb',vector='Diagnosis_last')
            self.draw_plots(best_models_wb,time_passed,orig_label,info='wb_mae',last_label=last_label,streamlit=wbst)
        except:pass





    def draw_plots(self,best_models,time_passed,orig_label,info='accuracy',last_label=None,streamlit=st):

        time_passed_scores=[]
        conversion_scores=[]
        time_passed_sizes=[]
        conversion_sizes=[]
        correlations=[]
        correlations2=[]
        shap_values=[]
        shap_values_no_fselinfo=[]
        shap_xgb_values=[]
        shap_xgb_no_fselinfo=[]
        feature_selection_used=[]
        individual_scores=self.get_individual_scores(info=info,features_to_use=best_models['linsvm'].feature_names)
        selected_features=self.get_features_selected(info=info,modelname=best_models['linsvm'].settings['folder-name'])
        for model in list(best_models.keys()):
            name=best_models[model].settings['folder-name']
            if 'enet' in name:
                feature_selection_used.append(1)
            else:
                feature_selection_used.append(0)
                
            shap,shap2=self.get_shap_summary_values(best_models[model],model,info=info)
            shap_values.append(shap)
            shap_values_no_fselinfo.append(shap2)
            shapxgb,shapxgb2=self.get_shap_summary_values(best_models[model],model,info=info,xgb_only=True,ret_all=True)
            shap_xgb_values.append(shapxgb)
            shap_xgb_no_fselinfo.append(shapxgb2)
            tps1,tps2=self.time_passed_performance_plot(best_models[model],time_passed,info=info)
            
            cs,cs2=self.conversion_performance_plot(best_models[model],orig_label,info=info,last_label=last_label)
            conversion_scores.append(cs)
            conversion_sizes.append(cs2)

            pred=self._flatten(best_models[model].predictions)
            #if info=='accuracy':
            #    pred_proba=self._flatten(best_models[model].predictions_proba)
            #    correlations2.append(pred_proba)
            time_passed_scores.append(tps1)
            time_passed_sizes.append(tps2)

            correlations.append(pred)
        self.plot_comp(time_passed_scores,time_passed_sizes,list(best_models.keys()),streamlit=streamlit[0])
        try:self.plot_comp(conversion_scores,conversion_sizes,list(best_models.keys()),title='Conversions',streamlit=streamlit[0])
        except:pass
        self.predictor_correlation(list(best_models.keys()),correlations,correlations2,streamlit=streamlit[1])
        self.feature_importance_plots(shap_values,individual_scores,selected_features,best_models[model].feature_names,feature_selection_used,streamlit=streamlit[1])
        self.feature_importance_plots(shap_values_no_fselinfo,individual_scores,selected_features,best_models[model].feature_names,feature_selection_used,streamlit=streamlit[1])
        self.feature_importance_plots(shap_xgb_values,individual_scores,selected_features,best_models[model].feature_names,feature_selection_used,streamlit=streamlit[1],only_best=True)
        self.feature_importance_plots(shap_xgb_values,individual_scores,selected_features,best_models[model].feature_names,feature_selection_used,streamlit=streamlit[1],only_best=True,time_passed=time_passed)
        self.feature_importance_plots(shap_xgb_no_fselinfo,individual_scores,selected_features,best_models[model].feature_names,feature_selection_used,streamlit=streamlit[1],only_best=True)
        lclass=self.get_label_change_classes(orig_label,last_label)
        self.feature_importance_plots(shap_xgb_no_fselinfo,individual_scores,selected_features,best_models[model].feature_names,feature_selection_used,streamlit=streamlit[1],only_best=True,lclass=lclass)
        if info=='accuracy':
            #orig_label2=self.get_time_passed_test_vector(info='diagnosisdx3',vector='Diagnosis')
            #cs,cs2=self.conversion_performance_plot(self.get_custom_model('adn_tp_Diagnosis_dx3_xgboost_knn_cv_10_5_standardize_none_noneweighted'),orig_label2,info=info,last_label=last_label)
            #st.write(cs)
            #cs,cs2=self.conversion_performance_plot(self.get_custom_model('adn_tp_Diagnosis_dx3_xgboost_knn_cv_10_5_standardize_none_noneindividual'),orig_label2,info=info,last_label=last_label)
            #st.write(cs)
            #tps1,tps2=self.time_passed_performance_plot(self.get_custom_model('adn_tp_Diagnosis_xgboost_knn_cv_10_5_standardize_none_noneweighted'),time_passed,info=info)
            try:
                cs,cs2=self.conversion_performance_plot(self.get_custom_model('adn_tp_Diagnosis_xgboost_knn_cv_10_5_standardize_none_noneweighted'),orig_label,info=info,last_label=last_label)
                cs1,cs21=self.conversion_performance_plot(self.get_custom_model('adn_tp_Diagnosis_xgboost_knn_cv_10_5_standardize_none_noneweighted2'),orig_label,info=info,last_label=last_label)
                cs2,cs22=self.conversion_performance_plot(self.get_custom_model('adn_tp_Diagnosis_xgboost_knn_cv_10_5_standardize_none_noneweighted3'),orig_label,info=info,last_label=last_label)
                cs3,cs23=self.conversion_performance_plot(self.get_custom_model('adn_tp_Diagnosis_xgboost_knn_cv_10_5_standardize_none_noneindividual'),orig_label,info=info,last_label=last_label)
                #self.plot_comp([tps1,tps1],[tps2,tps2],['xgboost','xgboost'],streamlit=streamlit)
                self.plot_comp([cs3,cs,cs1,cs2],[cs23,cs2,cs21,cs22],['Normal','All type weighted','Conv only weighted','All type weighted *2'],title='Conversions',streamlit=streamlit[0])
            except:pass

    def feature_importance_plots(self,shap,individual,selected_features,feature_names,feature_selection_used,streamlit=st,only_best=False,time_passed=[],lclass=[]):
        from sklearn.preprocessing import minmax_scale
        for i in range(len(feature_names)):
            try:
                temp_fname=feature_names[i][:25]
                if temp_fname in feature_names and len(temp_fname)>=25:
                    temp_fname += '_2'
                feature_names[i] = temp_fname
            except:
                pass
        feature_selection_used=np.asarray(feature_selection_used)
        fselused=len(feature_selection_used[feature_selection_used==1])
        selected_features_keep=selected_features
        selected_features=selected_features*fselused
        selected_features+=10*(9-fselused)
        selected_features=(selected_features/max(selected_features))*100
        if only_best:
            try:
                for j in range(len(shap)):
                    try:
                        if np.isnan(shap[j]).all():
                            continue
                        else:
                            shap=shap[j]
                            fselused=feature_selection_used[j]
                            break

                    except:
                        shap=shap[j]
                        fselused=feature_selection_used[j]
                        break

            except:
                shap=shap[0]
                fselused=feature_selection_used[0]
            if fselused==1:
                selected_features=selected_features_keep*10
            else:
                selected_features=np.ones((len(selected_features)))
                selected_features[selected_features==1]=100
            shap=np.vstack((shap[0],shap[1],shap[2],shap[3],shap[4],shap[5],
                shap[6],shap[7],shap[8],shap[9]))
            shap=np.abs(shap)
        for i in range(len(shap)):
            shap[i]=minmax_scale(shap[i],(0,100))
        if not time_passed and not lclass:
            shap_std=np.nanstd(shap,axis=0)
            shap=np.nanmean(shap,axis=0)
            
            sort_args=np.flip(np.lexsort((shap,selected_features)))
            
            fig_all = go.Figure(data=[
            go.Bar(name='Selected features', x=feature_names[sort_args], y=selected_features[sort_args][:150]),
            go.Bar(name='Individual scores', x=feature_names[sort_args], y=np.asarray(individual)[sort_args][:150]),
            go.Bar(name='SHAP values', x=feature_names[sort_args], y=np.asarray(shap)[sort_args][:150],error_y=dict(type='data', array=shap_std,thickness=1,width=2))

            ])

        else:
            if not time_passed:
                lclass=np.asarray(lclass)
                shap_std1=np.nanstd(shap[lclass==0],axis=0)
                shap1=np.nanmean(shap[lclass==0],axis=0)
                shap_std2=np.nanstd(shap[lclass==1],axis=0)
                shap2=np.nanmean(shap[lclass==1],axis=0)
                shap_std3=np.nanstd(shap[lclass==2],axis=0)
                shap3=np.nanmean(shap[lclass==2],axis=0)
                shap_std4=np.nanstd(shap[lclass==3],axis=0)
                shap4=np.nanmean(shap[lclass==3],axis=0)
                
                sort_args=np.flip(np.lexsort((shap3,selected_features)))
                
                fig_all = go.Figure(data=[
                go.Bar(name='Selected features', x=feature_names[sort_args], y=selected_features[sort_args][:150]),
                go.Bar(name='Individual scores', x=feature_names[sort_args], y=np.asarray(individual)[sort_args][:150]),
                go.Bar(name='SHAP values (Stable)', x=feature_names[sort_args], y=np.asarray(shap1)[sort_args][:150],error_y=dict(type='data', array=shap_std1,thickness=1,width=2)),
                go.Bar(name='SHAP values (Conv CN to MCI)', x=feature_names[sort_args], y=np.asarray(shap2)[sort_args][:150],error_y=dict(type='data', array=shap_std2,thickness=1,width=2)),
                go.Bar(name='SHAP values (Conv MCI to AD)', x=feature_names[sort_args], y=np.asarray(shap3)[sort_args][:150],error_y=dict(type='data', array=shap_std3,thickness=1,width=2)),
                go.Bar(name='SHAP values (CONV CN to AD)', x=feature_names[sort_args], y=np.asarray(shap4)[sort_args][:150],error_y=dict(type='data', array=shap_std4,thickness=1,width=2))

            ])
            else:
                time_passed=np.asarray(time_passed)/365.25/24/3600
                shap_std1=np.nanstd(shap[time_passed<3.5],axis=0)
                shap1=np.nanmean(shap[time_passed<3.5],axis=0)
                shap_std2=np.nanstd(shap[time_passed>3.5],axis=0)
                shap2=np.nanmean(shap[time_passed>3.5],axis=0)
                
                sort_args=np.flip(np.lexsort((shap2,selected_features)))
                
                fig_all = go.Figure(data=[
                go.Bar(name='Selected features', x=feature_names[sort_args], y=selected_features[sort_args][:150]),
                go.Bar(name='Individual scores', x=feature_names[sort_args], y=np.asarray(individual)[sort_args][:150]),
                go.Bar(name='SHAP values (<3.5 yrs)', x=feature_names[sort_args], y=np.asarray(shap1)[sort_args][:150],error_y=dict(type='data', array=shap_std1,thickness=1,width=2)),
                go.Bar(name='SHAP values (>3.5 yrs)', x=feature_names[sort_args], y=np.asarray(shap2)[sort_args][:150],error_y=dict(type='data', array=shap_std2,thickness=1,width=2))

            ])

        fig_all.update_layout(title=f' Feature Importances -',barmode='group',template='plotly_dark',width=1200)
        fig_all.update_xaxes( tickfont=dict( size=8))
        fig_all.update_yaxes(range=(0,100))
        #fig_all.update_layout({'plot_bgcolor': 'rgba(0, 0, 0, 0)','paper_bgcolor': 'rgba(0, 0, 0, 0)'})
        
        streamlit.write(fig_all)
        #fig_all.write_html('wb_xgb.html')
        #self.plot_comp(scores2,sizes2,modelnames,title='Conversions')
    def get_custom_model(self,folder_name):
        model_info_dict = pickle.load( open( f"./results/{folder_name}/model_info.p", "rb" ) )
        model_info=ModelInfo(loadfromfile=model_info_dict)
        return model_info
    def cluster_shap(self,shap,feature_names,orig_label,last_label,streamlit=st):
        for i in range(len(feature_names)):
            try:
                temp_fname=feature_names[i][:15]
                if temp_fname in feature_names and len(temp_fname)>=15:
                    temp_fname += '_2'
                feature_names[i] = temp_fname
            except:
                pass
        last_label_new=[]
        for i in range(len(last_label)):
            for j in range(len(last_label[i])):
                last_label_new.append(last_label[i][j])
        last_label=np.asarray(last_label_new)
        shap=shap[orig_label<last_label,:]
        from sklearn.cluster import AgglomerativeClustering,KMeans
        from plotly.subplots import make_subplots
        fig = make_subplots(
            rows=4, cols=6,vertical_spacing=0.15)
            #specs=[
            #    [{"colspan": 4}, {"colspan": 4},{"colspan": 4},None,None,None,None,None,None,None,None,None],
            #    [{"colspan": 1}, {"colspan": 1},{"colspan": 1},{"colspan": 1},None,None,None,None,None,None,None,None],
            #    [{"colspan": 1}, {"colspan": 1},{"colspan": 1},{"colspan": 1},{"colspan": 1},None],
            #    [{"colspan": 1}, {"colspan": 1},{"colspan": 1},{"colspan":1},{"colspan": 1},{"colspan": 1}],
                
            #    ])
        row_idx=1
        for i in range(3,7):
            cluster=AgglomerativeClustering(n_clusters=i)
            shap_clusters=cluster.fit_predict(shap)
            clstr,counts=np.unique(shap_clusters,return_counts=True)
            labels=np.unique(shap_clusters)
            col_idx=1
            for l in labels:
                t_shap=shap[shap_clusters==l]
                t_shap=np.mean(np.abs(t_shap), axis=0)
                arg=np.flip(np.argsort(t_shap))
                t_shap=t_shap[arg]
                t_feature_names=feature_names[arg]
                print(t_feature_names[:10])
                fig.add_trace(go.Bar(name=f'{counts[col_idx-1]} samples', x=t_feature_names[:10], y=t_shap[:10]),row=row_idx,col=col_idx)
                col_idx+=1
            row_idx+=1
        fig.update_xaxes( tickfont=dict( size=9))
        fig.update_layout(title=f' Hierarchical Shap Cluster -',barmode='group',template='plotly_dark',width=1200)
        streamlit.write(fig)
    def factor_shap(self,shap,feature_names):
        import pandas as pd
        
        from sklearn.decomposition import SparsePCA
        from factor_analyzer import FactorAnalyzer
        fa=FactorAnalyzer(rotation='varimax',n_factors=5,method='ml')
        table=self._loading_analysis(fa,shap,feature_names)
        pc=SparsePCA(random_state=0,n_components=5)
        table=self._loading_analysis(pc,shap,feature_names)
        print(1)
    def _loading_analysis(self,method,data,feature_names):
        method.fit(data)
        try:
            method.components_=method.loadings_.T
        except:pass
        components_variances=[]
        component_data=[]
        for i in range(len(method.components_)):
                        component_data.append([x for _, x in sorted(zip(abs(method.components_[i]), feature_names), reverse=True)])
                        component_data.append(sorted(abs(method.components_[i]), reverse=True))
                        try:
                            components_variances.append(["Component "+str(i), str(method.explained_variance_ratio_[i])])
                        except:
                            components_variances.append(["Component "+str(i), 'nan'])
        dataf=pd.DataFrame(data=np.asarray(component_data).transpose(), columns=np.asarray(components_variances).flatten())
        return dataf
        