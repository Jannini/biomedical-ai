import numpy as np
from scipy.stats import pearsonr,spearmanr
from scipy.cluster import hierarchy
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import PIL
import plotly.figure_factory as ff
import plotly.graph_objects as go
import pandas as pd
from sklearn.decomposition import SparsePCA
from factor_analyzer import FactorAnalyzer
import traceback
from biomedical_ai.interpretation.PredictiveTreeInteraction import PredictiveTreeInteraction
class Exploration():
    def __init__(self,data,labels,feature_names=[],settings=None,groups=None):
        self.data=data
        self.labels=labels
        self.feature_names=feature_names
        self.settings=settings
        self.groups=groups
        self.plot=True
        self.save_folder='./results_eda/'
        self.plot_type='plotly'
    def pred_tree_interaction(self):
        pti=PredictiveTreeInteraction()
        pti.fit(self.data,self.labels,self.feature_names)
        int_scores,int_names=pti.rank_interaction()
        self.pred_tree_int={'interaction_scores':int_scores,'interaction_names':int_names}
        self.pred_tree_table=pd.DataFrame(np.vstack((int_scores[:25],int_names[:25])).T,columns=['Balanced accuracy score increase','Feature combination'])
    def dendrogram(self):
        self.dendrogram_plots={}
        #Whole dendrogram
        from scipy.cluster.hierarchy import ward,weighted
        plot=ff.create_dendrogram(self.data.T,orientation='left',labels=list(self.feature_names),linkagefun=ward)
        plot.update_layout(title="Ward linkage")
        self.dendrogram_plots['all']=plot
        #Labelwise dendrogram
        for label in np.unique(self.labels):
            temp_data=self.data[self.labels==label]
            plot=ff.create_dendrogram(temp_data.T,orientation='left',labels=list(self.feature_names),linkagefun=ward)
            plot.update_layout(title=f"Ward linkage - Label: {label}")
            self.dendrogram_plots[f'label_{label}']=plot
        #Groupwise dendrogram
        try:
            if self.groups==None:return
        except:pass
        for group in np.unique(self.groups):
            temp_data=self.data[self.groups==group]
            plot=ff.create_dendrogram(temp_data.T,orientation='left',labels=list(self.feature_names),linkagefun=ward)
            plot.update_layout(title=f"Ward linkage - Group: {group}")
            self.dendrogram_plots[f'group_{group}']=plot
    
    def _loading_analysis(self,method,data):
        method.fit(data)
        try:
            method.components_=method.loadings_.T
        except:pass
        components_variances=[]
        component_data=[]
        for i in range(len(method.components_)):
                        component_data.append([x for _, x in sorted(zip(abs(method.components_[i]), self.feature_names), reverse=True)])
                        component_data.append(sorted(abs(method.components_[i]), reverse=True))
                        try:
                            components_variances.append(["Component "+str(i), str(method.explained_variance_ratio_[i])])
                        except:
                            components_variances.append(["Component "+str(i), 'nan'])
        dataf=pd.DataFrame(data=np.asarray(component_data).transpose(), columns=np.asarray(components_variances).flatten())
        return dataf
        
    def sparse_pca(self,components=4):
        self.pca_tables={}
        self.pca_tables_titles=[]
        pc=SparsePCA(random_state=0,n_components=components)
        table=self._loading_analysis(pc,self.data)
        self.pca_tables_titles.append("Sparse PCA")
        self.pca_tables['all']=table
        #Labelwise
        for label in np.unique(self.labels):
            temp_data=self.data[self.labels==label]
            table=self._loading_analysis(pc,temp_data)
            self.pca_tables_titles.append(f"Sparse PCA - Label {label}")
            self.pca_tables[f'label_{label}']=table
        #Groupwise
        try:
            if self.groups==None:return
        except:pass
        for group in np.unique(self.groups):
            temp_data=self.data[self.groups==group]
            table=self._loading_analysis(pc,temp_data)
            self.pca_tables_titles.append(f"Sparse PCA - Group {group}")
            self.pca_tables[f'group_{group}']=table
    def factor_analysis(self,components=4):
        self.fa_tables={}
        self.fa_tables_titles=[]
        fa=FactorAnalyzer(rotation='varimax',n_factors=components,method='ml')
        table=self._loading_analysis(fa,self.data)
        self.fa_tables_titles.append(f"Factor Analysis with varimax")
        self.fa_tables['all']=table
        #Labelwise
        for label in np.unique(self.labels):
            temp_data=self.data[self.labels==label]
            table=self._loading_analysis(fa,temp_data)
            self.fa_tables_titles.append(f"Factor Analysis with varimax - Label {label}")
            self.fa_tables[f'label_{label}']=table
        #Groupwise
        try:
            if self.groups==None:return
        except:pass
        for group in np.unique(self.groups):
            temp_data=self.data[self.groups==group]
            table=self._loading_analysis(fa,temp_data)
            self.fa_tables_titles.append(f"Factor Analysis with varimax - Group {group}")
            self.fa_tables[f'group_{group}']=table
    def hypothesis_testing(self):
        labels=np.reshape(self.labels,(self.labels.shape[0],1))
        df=pd.DataFrame(np.hstack((self.data,labels)),columns=np.hstack((self.feature_names,'label')))
        import pingouin as pg
        self.hypothesis_test_data={}
        for i in self.feature_names:
            self.hypothesis_test_data[i]={}
            anova=pg.anova(df,'label',i)
            try:
                temp=anova['F']
            except:
                anova={'F':np.nan,'p-unc':np.nan,'np2':np.nan}
            self.hypothesis_test_data[i]['anova']=anova
            kruskal=pg.kruskal(df,'label',i)
            normal=pg.normality(df[i])
            self.hypothesis_test_data[i]['kruskal']=kruskal
            self.hypothesis_test_data[i]['normal']=normal
            for j in np.unique(self.labels):
                normal1=pg.normality(df[i][df['label']==j])
                import scipy.stats as ss
                mn1=np.nanmean(list(df[i][df['label']==j]))
                std1=np.nanstd(list(df[i][df['label']==j]))
                med1=np.nanmedian(list(df[i][df['label']==j]))
                iqr1=ss.iqr(list(df[i][df['label']==j]),nan_policy='omit')

                self.hypothesis_test_data[i][f'normal_{j}']=normal1
                self.hypothesis_test_data[i][f'mean_{j}']=mn1
                self.hypothesis_test_data[i][f'median_{j}']=med1
                self.hypothesis_test_data[i][f'std_{j}']=std1
                self.hypothesis_test_data[i][f'iqr_{j}']=iqr1
            for j in np.unique(self.labels):
                for k in np.unique(self.labels):
                    if k>=j:continue
                    ttest_12=pg.ttest(df[i][df['label']==j],df[i][df['label']==k])
                    mwu_12=pg.mwu(df[i][df['label']==j],df[i][df['label']==k])
                    self.hypothesis_test_data[i][f'ttest_{j}_{k}']=ttest_12
                    self.hypothesis_test_data[i][f'mwutest_{j}_{k}']=mwu_12
        self.hypothesis_test_table=pd.DataFrame(self.hypothesis_test_data.keys(),columns=['Features'])
        for j in self.hypothesis_test_data[i].keys():
            temp_data=[]
            for k in self.hypothesis_test_data.keys():
                try:
                    temp_data.append(float(self.hypothesis_test_data[k][j]['p-unc']))
                except:
                    try:
                        temp_data.append(float(self.hypothesis_test_data[k][j]['pval']))
                    except:
                        try:
                            temp_data.append(float(self.hypothesis_test_data[k][j]['p-val']))
                        except:
                            temp_data.append(float(self.hypothesis_test_data[k][j]))
            self.hypothesis_test_table[j]=temp_data
    def graph_analysis(self):
        from biomedical_ai.interpretation.network.GraphML import GraphML
        self.networks={}
        gml=GraphML(self.data,self.labels,self.feature_names)
        graph,graph_plot=gml.construct_graph_bayes()
        self.networks['all_bayes']=graph
        self.networks['all_bayes_plot']=graph_plot

        graph,graph_plot=gml.construct_graph_partial_correlation()
        self.networks['all_glasso']=graph
        self.networks['all_glasso_plot']=graph_plot
        #Labelwise
        for label in np.unique(self.labels):
            temp_data=self.data[self.labels==label]
            temp_labels=self.labels[self.labels==label]
            gml=GraphML(temp_data,temp_labels,self.feature_names)
            try:
                graph,graph_plot=gml.construct_graph_bayes()
                self.networks[f'label_bayes_{label}']=graph
                self.networks[f'label_bayes_plot_{label}']=graph_plot
            except:pass

            try:
                graph,graph_plot=gml.construct_graph_partial_correlation()
                self.networks[f'label_glasso_{label}']=graph
                self.networks[f'label_glasso_plot_{label}']=graph_plot
            except:pass
        #Groupwise
        try:
            if self.groups==None:return
        except:pass
        for group in np.unique(self.groups):
            temp_data=self.data[self.groups==group]
            temp_labels=self.labels[self.labels==label]
            gml=GraphML(temp_data,temp_labels,self.feature_names)
            try:
                graph,graph_plot=gml.construct_graph_bayes()
                self.networks[f'group_bayes_{group}']=graph
                self.networks[f'group_bayes_plot_{group}']=graph_plot
            except:pass
            
            try:

                graph,graph_plot=gml.construct_graph_partial_correlation()
                self.networks[f'group_glasso_{group}']=graph
                self.networks[f'group_glasso_plot_{group}']=graph_plot
            except:pass
    def basic(self):
        labels=np.reshape(self.labels,(self.labels.shape[0],1))
        df=pd.DataFrame(np.hstack((self.data,labels)),columns=np.hstack((self.feature_names,'label')))
        self.basic_data={}
        self.basic_data['pd_describe']=df.describe()
        percent_missing = df.isnull().sum() * 100 / len(df)
        missing_value_df = pd.DataFrame({'Feature name': df.columns,
                                 'Percent Missing': percent_missing})
        missing_value_df=missing_value_df.sort_values('Percent Missing')
        self.basic_data['pd_info']=missing_value_df
        memory_usage=df.memory_usage().sum()
        self.basic_data['pd_memory']=memory_usage
        
        
    def distribution(self):
        labels=np.reshape(self.labels,(self.labels.shape[0],1))
        df=pd.DataFrame(np.hstack((self.data,labels)),columns=np.hstack((self.feature_names,'label')))
        self.distribution_figures=[]
        import seaborn as sns
        for i in self.feature_names:
            colorby='label'

            try:
                if i=='Unnamed: 0' or i=='index' or i=='RID' or i=='Diagnosis_last':
                    continue
                #fig, axes = plt.subplots(1, 2)

                figure( figsize=(10, 6), dpi=300)
                #sns.color_palette()
                #plt.title(i)
                fig=sns.displot(df, x=i, hue=colorby, kind="kde", fill=True,palette='tab10',height=6.5, aspect=11/6.5)
                plt.tight_layout()
                fig.fig.tight_layout()
                image=PIL.Image.frombytes('RGB', fig.fig.canvas.get_width_height(),fig.fig.canvas.tostring_rgb())
                self.distribution_figures.append(image)
                
                plt.clf()
            except:pass
            
    def correlation(self):
        self.pearson={}
        self.spearman={}
        pearson_correlation_matrix=np.ones((len(self.feature_names),len(self.feature_names)))
        spearman_correlation_matrix=np.ones((len(self.feature_names),len(self.feature_names)))
        for i in range(len(self.feature_names)):
            for j in range(len(self.feature_names)):
                if j>i:continue
                pearson_correlation_matrix[i,j]=pearsonr(self.data[:,i],self.data[:,j])[1]
                spearman_correlation_matrix[i,j]=spearmanr(self.data[:,i],self.data[:,j])[1]
        fig_pearson = go.Figure(data=go.Heatmap(z=pearson_correlation_matrix,x=self.feature_names,y=self.feature_names,hoverongaps = False))
        fig_spearman = go.Figure(data=go.Heatmap(z=spearman_correlation_matrix,x=self.feature_names,y=self.feature_names,hoverongaps = False))
        fig_spearman.update_layout(title=f"Spearman")
        fig_pearson.update_layout(title=f"Pearson")
        self.pearson['all']=fig_pearson
        self.spearman['all']=fig_spearman
        for label in np.unique(self.labels):
            temp_data=self.data[self.labels==label]
            pearson_correlation_matrix=np.ones((len(self.feature_names),len(self.feature_names)))
            spearman_correlation_matrix=np.ones((len(self.feature_names),len(self.feature_names)))
            for i in range(len(self.feature_names)):
                for j in range(len(self.feature_names)):
                    if j>i:continue
                    pearson_correlation_matrix[i,j]=pearsonr(temp_data[:,i],temp_data[:,j])[1]
                    spearman_correlation_matrix[i,j]=spearmanr(temp_data[:,i],temp_data[:,j])[1]
            
            fig_pearson = go.Figure(data=go.Heatmap(z=pearson_correlation_matrix,x=self.feature_names,y=self.feature_names,hoverongaps = False))
            fig_spearman = go.Figure(data=go.Heatmap(z=spearman_correlation_matrix,x=self.feature_names,y=self.feature_names,hoverongaps = False))
            fig_spearman.update_layout(title=f"Spearman - Label {label}")
            fig_pearson.update_layout(title=f"Pearson - Label {label}")
            self.pearson[f'label_{label}']=fig_pearson
            self.spearman[f'label_{label}']=fig_spearman
        try:
            if self.groups==None:return
        except:pass
        for group in np.unique(self.groups):
            temp_data=self.data[self.groups==group]
            pearson_correlation_matrix=np.ones((len(self.feature_names),len(self.feature_names)))
            spearman_correlation_matrix=np.ones((len(self.feature_names),len(self.feature_names)))
            for i in range(len(self.feature_names)):
                for j in range(len(self.feature_names)):
                    if j>i:continue
                    pearson_correlation_matrix[i,j]=pearsonr(temp_data[:,i],temp_data[:,i])
                    spearman_correlation_matrix[i,j]=spearmanr(temp_data[:,i],temp_data[:,i])
            
            fig_pearson = go.Figure(data=go.Heatmap(z=pearson_correlation_matrix,x=self.feature_names,y=self.feature_names,hoverongaps = False))
            fig_spearman = go.Figure(data=go.Heatmap(z=spearman_correlation_matrix,x=self.feature_names,y=self.feature_names,hoverongaps = False))
            fig_spearman.update_layout(title=f"Spearman - Group {group}")
            fig_pearson.update_layout(title=f"Pearson - Group {group}")
            
            self.pearson[f'group_{group}']=fig_pearson
            self.spearman[f'group_{group}']=fig_spearman
    def explore(self):
        # TODO add graph analysis --> dependencies are needed
        # try:self.graph_analysis()
        # except Exception:
            
        #     traceback.print_exc()
        try:self.basic()
        except Exception:
            
            traceback.print_exc()
        try:self.distribution()
        except Exception:
            
            traceback.print_exc()
        try:self.dendrogram()
        except Exception:
            
            traceback.print_exc()
        try:self.pred_tree_interaction()
        except Exception:
            
            traceback.print_exc()
        try:self.sparse_pca()
        except Exception:
            
            traceback.print_exc()
        try:self.factor_analysis()
        except Exception:
            
            traceback.print_exc()
        try:self.correlation()
        except Exception:
            
            traceback.print_exc()
        try:self.hypothesis_testing()
        except Exception:
            
            traceback.print_exc()

