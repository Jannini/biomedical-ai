import paramiko
class SSHCluster():
    def __init__(self,machine_names,host_names,usernames,passwords,local_dirs,conda_envs,worker_processes,conda_type) -> None:
        
        self.machine_names=machine_names
        self.host_names=host_names
        self.usernames=usernames
        self.passwords=passwords
        self.local_dirs=local_dirs
        self.conda_envs=conda_envs
        self.worker_threads=worker_processes
        self.conda_types=conda_type
    def create_cluster(self):
        ssh_client =paramiko.SSHClient()
        dask_folder=f"~/{self.conda_types[0]}/envs/{self.conda_envs[0]}/lib/python3.7/site-packages/distributed/cli/"
        python=f"~/{self.conda_types[0]}/envs/{self.conda_envs[0]}/bin/python"
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=self.host_names[0],username=self.usernames[0],password=self.passwords[0])
        #channel=ssh_client.invoke_shell()
        stdin, stdout, stderr = ssh_client.exec_command(f"cd {self.local_dirs[0]} ; source ~/{self.conda_types[0]}/bin/activate {self.conda_envs[0]} ; \
                                                         nohup {python} {dask_folder}dask_scheduler.py > /dev/null 2>&1 &") 


        #print(stdout.readlines())
        #print(stderr.readlines())
        ssh_client.close()
        for i in range(len(self.host_names)):
            ssh_client =paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh_client.connect(hostname=self.host_names[i],username=self.usernames[i],password=self.passwords[i])
            dask_folder=f"~/{self.conda_types[i]}/envs/{self.conda_envs[0]}/lib/python3.7/site-packages/distributed/cli/"
            python=f"~/{self.conda_types[i]}/envs/{self.conda_envs[0]}/bin/python"
            stdin, stdout, stderr = ssh_client.exec_command(f"cd {self.local_dirs[i]} & source ~/{self.conda_types[i]}/bin/activate {self.conda_envs[i]} & \
                                                        nohup {python} {dask_folder}dask_worker.py 10.200.91.26:8786 --nprocs {self.worker_threads[i]} --nthreads 1 \
                                                        --name {self.machine_names[i]} > /dev/null 2>&1 &")
            ssh_client.close()

def shutdown_dask(port=8786):
    port=port
    try:
        client=Client(f"127.0.0.1:{port}")
        print(f"Shutting down Dask at {port}")
        client.shutdown()
    except:
        print(f"Dask at {port} already shut down")
        
if __name__ == "__main__":
    pass
    #shutdown_dask()