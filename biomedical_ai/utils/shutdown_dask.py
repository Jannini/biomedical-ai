from dask.distributed import Client
if __name__ == "__main__":
    port=8786
    try:
        client=Client(f"127.0.0.1:{port}")
        print(f"Shutting down Dask at {port}")
        client.shutdown()
    except:
        print(f"Dask at {port} already shut down")


#import os
#my_worker = 'name-or-ip-address-to-worker'
#client.run(lamda: os._exit(
# 0), workers=[my_worker])