from math import factorial
import numpy as np
from collections import Counter, defaultdict
from sklearn.utils import check_random_state
import pickle
import os
import pandas as pd
def save_model(model,modelname):
    with open(f'{modelname}.pickle', 'wb') as file:
        pickle.dump(model, file)
def is_equal(string1,string2):
    if isinstance(string1,list):string1=string1[0]
    if isinstance(string2,list):string2=string2[0]
    equal=False
    if string1.lower() == string2.lower(): equal=True
    return equal
def calculate_valid_settings(settings):
    all_settings=[]
    for i in settings['FILENAME']:
        for j in settings['PREDICTOR']:
            for k in settings['CV']:
                for l in settings['NORMALIZATION']:
                    for o in settings['HYPERPARAMETER OPTIMIZATION']['METHOD']:
                            for h in settings['FEATURE SELECTION']['METHOD']:
                                    for m in settings['IMPUTATION']:
                                        temp_setting=settings.copy()
                                        temp_setting['HYPERPARAMETER OPTIMIZATION']=settings['HYPERPARAMETER OPTIMIZATION'].copy()
                                        temp_setting['FEATURE SELECTION']=settings['FEATURE SELECTION'].copy()
                                        temp_setting['FILENAME']=i
                                        temp_setting['PREDICTOR']=j
                                        temp_setting['CV']=k
                                        temp_setting['NORMALIZATION']=l
                                        temp_setting['HYPERPARAMETER OPTIMIZATION']['METHOD']=o
                                        temp_setting['FEATURE SELECTION']['METHOD']=h
                                        temp_setting['IMPUTATION']=m
                                        all_settings.append(temp_setting)
    return all_settings
def load_data(filename):
    df=pd.read_csv(filename)
    feature_names=df.columns._data[:-1]
    data=df.values
    labels=data[:,-1]
    data=data[:,:-1]
    supplement_data={'feature_names':feature_names}
    return data,labels,supplement_data
    
def filterfilter(filters,a):
    skip_filter_all = False
    skip_filter_any = True
    skip_filter_none = False
    for f in filters['FILTERS_ALL']:
        if f not in a: skip_filter_all = True
    for f in filters['FILTERS_ANY']:
        if f in a: skip_filter_any = False
    for f in filters['FILTERS_NONE']:
        if f in a: skip_filter_none = True
    if not filters['FILTERS_ANY']:
        skip_filter_any=False
    skip=False
    if skip_filter_all or skip_filter_any or skip_filter_none:
        skip=True
    return skip
class Stratified1Fold():
    def __init__(self,test_id_file=''):
        self.test_set=[0]
    def split(self,X,y=None,groups=None):
        test_indices=self.test_set
        train_indices=[]
        for i in range(X.shape[0]):
            if i in test_indices:
                continue
            else:
                train_indices.append(i)
        yield train_indices,test_indices
class RepeatedStratifiedGroupKFold():

    def __init__(self, n_splits=10, n_repeats=1, random_state=0):
        self.n_splits = n_splits
        self.n_repeats = n_repeats
        self.random_state = random_state
        #self.groups=groups
        
    # Implementation based on this kaggle kernel:
    #    https://www.kaggle.com/jakubwasikowski/stratified-group-k-fold-cross-validation
    def split(self, X, y=None, groups=None):
        #if groups==None: groups=self.groups
        k = self.n_splits
        def eval_y_counts_per_fold(y_counts, fold):
            y_counts_per_fold[fold] += y_counts
            std_per_label = []
            for label in range(labels_num):
                label_std = np.std(
                    [y_counts_per_fold[i][label] / y_distr[label] for i in range(k)]
                )
                std_per_label.append(label_std)
            y_counts_per_fold[fold] -= y_counts
            return np.mean(std_per_label)
            
        rnd = check_random_state(self.random_state)
        for repeat in range(self.n_repeats):
            labels_num = np.max(y) + 1
            y_counts_per_group = defaultdict(lambda: np.zeros(labels_num))
            y_distr = Counter()
            for label, g in zip(y, groups):
                y_counts_per_group[g][label] += 1
                y_distr[label] += 1

            y_counts_per_fold = defaultdict(lambda: np.zeros(labels_num))
            groups_per_fold = defaultdict(set)
        
            groups_and_y_counts = list(y_counts_per_group.items())
            rnd.shuffle(groups_and_y_counts)

            for g, y_counts in sorted(groups_and_y_counts, key=lambda x: -np.std(x[1])):
                best_fold = None
                min_eval = None
                for i in range(k):
                    fold_eval = eval_y_counts_per_fold(y_counts, i)
                    if min_eval is None or fold_eval < min_eval:
                        min_eval = fold_eval
                        best_fold = i
                y_counts_per_fold[best_fold] += y_counts
                groups_per_fold[best_fold].add(g)

            all_groups = set(groups)
            for i in range(k):
                train_groups = all_groups - groups_per_fold[i]
                test_groups = groups_per_fold[i]

                train_indices = [i for i, g in enumerate(groups) if g in train_groups]
                test_indices = [i for i, g in enumerate(groups) if g in test_groups]

                yield train_indices, test_indices

def ordinalEncoder(data,labels):
    all_data={}
    all_labels={}
    for i in range(min(labels),max(labels)):
        all_data[str(i)]=[]
        all_labels[str(i)]=[]
    for i in range(min(labels),max(labels)):
        for j in range(len(labels)):
            if labels[j]<=i:
                all_data[str(i)].append(data[j,:])
                all_labels[str(i)].append(0)
            else:
                all_data[str(i)].append(data[j,:])
                all_labels[str(i)].append(1)
    return all_data,all_labels

def onevsrestEncoder(data,labels):
    all_data={}
    all_labels={}
    for i in range(min(labels),max(labels)+1):
        all_data[str(i)]=[]
        all_labels[str(i)]=[]
    for i in range(min(labels),max(labels)+1):
        for j in range(len(labels)):
            if labels[j]==i:
                all_data[str(i)].append(data[j,:])
                all_labels[str(i)].append(1)
            else:
                all_data[str(i)].append(data[j,:])
                all_labels[str(i)].append(0)
    return all_data,all_labels
def onevsoneEncoder(data,labels,groups):
    all_data={}
    all_labels={}
    all_groups={}
    class_combs_to_keep=[1,5]
    for i in range(min(labels),factorial(len(np.unique(labels))-1)):
        if i in class_combs_to_keep:
            all_data[str(i)]=[]
            all_labels[str(i)]=[]
            all_groups[str(i)]=[]
    if max(labels)>len(labels)-1: print('Probably not going to work properly')
    counter=0
    class_combs=[]
    
    for i in range(len(np.unique(labels))):
        for j in range(len(np.unique(labels))):
            if j>i:
                for d in range(len(data)):
                    if (labels[d]==i or labels[d]==j) and counter in class_combs_to_keep:
                        all_data[str(counter)].append(data[d,:])
                        all_labels[str(counter)].append(labels[d])
                        all_groups[str(counter)].append(groups[d])
                counter+=1
                class_combs.append((i,j))
            else:
                continue
    return all_data,all_labels,all_groups

