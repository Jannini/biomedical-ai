import json
import pickle
import numpy as np
import scipy.stats as sstats


class ModelInfo():


    def __init__(self,settings=None,loadfromfile=None):
        if loadfromfile!=None:
            for key in loadfromfile: 
                setattr(self, key, loadfromfile[key])
        else:
            self.settings=settings


            self.best_pipeline_model=None
            self.selected_features=[]
            self.outlier_model=None
            self.name=''

            self.opt_hyperparams=[]
            self.index={'og_index':[],'train_indexes':[],'test_indexes':[]}
            self.scores={}
            self.shap_values=[]
            self.shap_explainers=[]

            
            self.best_pipeline_models=[]
            self.all_pipeline_models=[]
            self.feature_names=None
            self.models=[]
            self.model=None
            self.temp_plots=[]
    def save(self,save_folder,save_name=''):
        with open(f'{save_folder}/{save_name}.p','wb') as f:
            pickle.dump(self.__dict__,f)
    def saveJSON(self):
        import json
        with open(f'./results/{self.settings["folder-name"]}/model_info.json','w') as f:
            json.dump(self.__dict__,f,indent=6)

    def getstats(self):
        scores_stats={}
        for i in self.scores.keys():
            try:
                max=np.max(self.scores[i])
                min=np.min(self.scores[i])
                med=np.median(self.scores[i])
                mean=np.mean(self.scores[i])
                std=np.std(self.scores[i])
                iqr=sstats.iqr(self.scores[i])
                scores_stats[i]=[min,max,med,mean,iqr,std]
            except:
                continue
        scores_overfit_stats={}
        for i in self.scores_overfit.keys():
            try:
                max=np.max(self.scores_overfit[i])
                min=np.min(self.scores_overfit[i])
                med=np.median(self.scores_overfit[i])
                mean=np.mean(self.scores_overfit[i])
                std=np.std(self.scores_overfit[i])
                iqr=sstats.iqr(self.scores_overfit[i])
                scores_stats[i]=[min,max,med,mean,iqr,std]
            except:
                continue
    def save_models(self):
        cv_idx=1
        for model in self.models:
            model.save_model('./results/'+self.settings['folder-name']+'/model/model_'+str(cv_idx)+'.json')
            config=model.save_config()
            with open('./results/'+self.settings['folder-name']+'/model/modelconfig_'+str(cv_idx)+'.json','w') as f:
                f.write(config)
            cv_idx+=1

    def equals(self,model: any) -> bool:
        pass

