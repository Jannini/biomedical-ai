import mlflow
import platform
import os
import time
from datetime import datetime,timedelta


COMPLETED_NUM=0
def save_callback(model_info):
    global COMPLETED_NUM
    COMPLETED_NUM+=1
    
    model_info=model_info.result()
    total_num=model_info.settings['total_num_of_settings']
    logfile=open('./results/'+model_info.settings['folder-name']+'/logging.log','w')
    print(f"Finished {model_info.settings['folder-name']}\t [{COMPLETED_NUM}/{total_num}]\t {round((COMPLETED_NUM/total_num)*100,2)}% completed (BAS: {round(np.mean(model_info.scores['BAS']),2)})")
    log('Date and time:',logfile)
    log(model_info.settings['dt_string'],logfile)
    log('System info:',logfile)
    log(platform.platform(),logfile)
    log(platform.system(),logfile)
    log(platform.processor(),logfile)
    log(platform.architecture(),logfile)
    log('Starting with settings: ',logfile)
    log('ML-Model:\t'+model_info.settings['ml-model'],logfile)
    log('Inference strategy:\t'+model_info.settings['ordinal-strategy'],logfile)
    log('Outer CV iterations:\t'+str(model_info.settings['cross-validation']),logfile)
    log('Inner CV iterations:\t'+str(model_info.settings['inner-cross-validation']),logfile)
    log('Hyperparameter optimization algorithm:\t'+str(model_info.settings['hyperparameter-optimization']),logfile)
    log('Preprocess algorithms:\t'+' & '.join(model_info.settings['preprocess-algorithms']),logfile)
    log('Saving to folder '+model_info.settings['folder-name'],logfile)
    end_time=time.time()
    runtime=end_time-model_info.settings['st_time']
    log('Finished in:',logfile,end='\t')
    log(str(timedelta(seconds=runtime)),logfile)
    model_info.save()
    #model_info.saveJSON()
    logfile.close()
    del model_info
def log_mlflow_cv_params(scores,hyperparameters):
    for score in scores.keys():
        mlflow.log_metric(score,scores[score][-1])
    try:mlflow.log_params(hyperparameters)
    except:pass
def log_mlflow_settings(settings):
    mlflow.log_params(settings)
def log_mlflow_global_scores(scores):
    pass

def logprint(text,file,end='\n'):
    print(text,file=file,flush=True,end=end)
    print(text,end=end)
def log(text,file,end='\n'):
    print(text,file=file,flush=True,end=end)