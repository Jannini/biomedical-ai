import numpy as np

def get_best_model(model_infos,score='BAS',maximize=True):
    best_score = None
    best_model = None
    for model_info in model_infos:
        temp_score =np.mean(model_info.scores[score])
        if best_score == None:
            best_score = temp_score
            best_model = model_info
        else:
            if maximize and temp_score > best_score:
                best_score = temp_score
                best_model = model_info
            elif not maximize and temp_score < best_score:
                best_score = temp_score
                best_model = model_info
    return best_model