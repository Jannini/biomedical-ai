import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="BioMedical AI",
    version="0.0.1",
    author="Janos Szalma",
    author_email="sz.janni@gmail.com",
    description="Wrapper package around multiple machine learning packages",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    project_urls={
        "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "biomedical_ai"},
    packages=setuptools.find_packages(where="biomedical_ai"),
    python_requires=">=3.7",
)