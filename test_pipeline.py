from biomedical_ai.settings.settings import get_test_settings
from biomedical_ai.prediction.model_creator import create_run_models



def test_eda_pipeline():
    pass

def test_classification_pipeline():
    settings=get_test_settings()
    model_infos=create_run_models(settings)
    assert len(model_infos) == 2
    assert len(model_infos[0].models) == 10

def test_regression_pipeline():
    pass

def test_clustering_pipeline():
    pass

