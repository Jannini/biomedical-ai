# Machine Learning & Data Exploration Pipeline
## Description
Specify all the pipeline settings (input data location, preprocessing, feature selection, predictor, cross-validation, metrics, etc.) and run all setting combinations. Warning: not all combinations are supported, errors may occur.
## Install Guide
1. Clone repository
2. Install miniconda/anaconda
3. From the root repository directory install the environment with conda create env -f environment.yml
4. Run pytest (just write pytest in the command line and press enter from the root directory) to make sure everything is set up correctly, all tests should pass
## User Guide
You can use the pipeline by running the run_ml_pipeline.py or run_eda_pipeline.py scripts.
This loads a settings file from the /biomedical_ai/settings folder (the settings.yaml in the case of the ml pipeline).
You can either edit this settings file or create your own and specify the location as a command line argument 
(for example: run_ml_pipeline.py /home/user/settings.yaml).
## Developer Guide
You can test the pipeline by running the run_ml_pipeline.py or run_eda_pipeline.py scripts.
This loads a settings file from the /biomedical_ai/settings folder (the settings.yaml in the case of the ml pipeline).
You can either edit this settings file or create your own and specify the location as a command line argument 
(for example: run_ml_pipeline.py /home/user/settings.yaml).
In the case of the ml pipeline the main script that calls the different pipeline steps is the 
/biomedical_ai/prediction/model_creator.py
To add new methods find the appropriate python file (for example to add a classifier use the 
/biomedical_ai/prediction/classifiers.py) and define a new classifier according to the class schema.
### Classification example
Main steps (fill in the settings file accordingly)
1. Loading data (utils/utils.py/load_data)
   
    CSV file where last column is the variable to predict
2. Cross validation (prediction/cross_validation.py)

    Use one available or add one using the same syntax
3. Normalization (prediction/preprocess.py)

    Use the Normalizer class to add new methods that already implement fit_transform and transform. (currently new else if statement in fit_transform)
4. Feature selection (prediction/feature_selection.py)

    Use the FeatureSelector class to add new methods that already implement fit_transform and transform. (currently new else if statement in fit_transform)
5. Classification (prediction/classification.py)

    Use the Classifier class to add new methods that already implement fit_transform and transform. (currently new else if statement in fit_transform)
6. Metric calulation (prediction/metrics.py)
    Add a new if statement using the already established syntax to add a new metric.
If the method is already implemented just add its name to the settings file.

## Folder structure
Root folder with main pipeline run and test scripts
- Decision Support
- Documentation
- Interpretation (hypothesis test, EDA, statistics, etc.)
- Miscellaneous
- Prediction (training, testing)
- Projects
- Settings
- Utils
