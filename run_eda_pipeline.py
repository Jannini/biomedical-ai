from biomedical_ai.settings.settings import get_eda_settings
import sys
from biomedical_ai.interpretation.EDA import Exploration
from biomedical_ai.utils.utils import load_data
from biomedical_ai.interpretation.visualization import visualize_EDA
import numpy as np
if __name__ == "__main__":
    if len(sys.argv)>1:
        fname=sys.argv[1]
    else:
        fname='None'
    settings=get_eda_settings(fname)
    data,labels,supplement_data=load_data("./biomedical_ai/miscellaneous/adn_tp_Diagnosis_cog.csv")
    # Since i was lazy to delete the few rows with nans:
    data=np.nan_to_num(data)
    # Since some eda modules need normalization and its not currently included:
    data=(data-np.mean(data,axis=0))/np.std(data,axis=0)
    eda=Exploration(data,labels,settings=settings,feature_names=supplement_data['feature_names'])
    eda.explore()
    visualize_EDA(eda)
    print('Done')