from biomedical_ai.settings.settings import get_settings
import sys
from biomedical_ai.prediction.model_creator import create_run_models
if __name__ == "__main":
    if len(sys.argv)>1:
        fname=sys.argv[1]
    else:
        fname='None'
    settings=get_settings(fname)
    model_info=create_run_models(settings)
    model_info.save('./results','test')